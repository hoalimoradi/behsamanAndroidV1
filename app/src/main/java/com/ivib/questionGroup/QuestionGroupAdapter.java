package com.ivib.questionGroup;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;

import java.util.List;

public class QuestionGroupAdapter extends RecyclerView.Adapter<QuestionGroupAdapter.ViewHolder> {

    private final OnItemClickListener listener;
    private List<Group> data;
    private Context context;

    QuestionGroupAdapter(Context context, List<Group> data, OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public QuestionGroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_question_group_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.click(data.get(position), listener);
        holder.group_name.setText(data.get(position).getName());

        // دریافت تعداد سوالات پاسخ داده نشده در گروه
        int result = isAllQuestionGroupAnswered(data.get(position).getId());
        if (result == 0) {
            holder.ic_notifications.setVisibility(View.GONE);
            holder.ic_playlist_add_check.setVisibility(View.VISIBLE);
        } else {
            holder.ic_playlist_add_check.setVisibility(View.GONE);
            holder.ic_notifications.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return  data == null ? 0 : data.size();

    }

    /**
     * بررسی تعداد تعداد سوالات پاسخ داده نشده در یک گروه
     *
     * @param groupId شناسه گروه
     * @return int تعداد سوالات پاسخ داده نشده
     */
    private int isAllQuestionGroupAnswered(String groupId) {
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
        dbQuestionsFileOperations dbQuestionFileOp = new dbQuestionsFileOperations(context);

        int qAll = 0;
        int qAnswered = 0;

        // آپدیت لیست سوالات مخفی شده
        globalVariable.updateGlobalCQL();

        // جستجو بر روی همه سوالات
        for (Question q : globalVariable.getGlobalQL()) {

            // اگر سوال در گروه مورد نظر باشد
            if (q.getQuestionGroupId().equals(groupId)) {
                qAll = qAll + 1;

                // اگر سوال مخفی شده باشد
                if (globalVariable.getGlobalCQL().contains(q.getId())) {
                    qAnswered = qAnswered + 1;
                } else {
                    // اگر به سوال پاسخ داده شده است
                    if (dbQuestionOp.IsExist(globalVariable.getCheckListInfoId(), q.getId())) {

                        // اگر سوال دارای مستندات اجباری باشد
                        if (q.getQuestionDocumentStatus() == 1) {

                            // اگر برای سوال مستندات اضافه شده باشد
                            if (dbQuestionFileOp.IsExist(globalVariable.getCheckListInfoId(), q.getId())) {
                                qAnswered = qAnswered + 1;
                            }
                        } else {
                            qAnswered = qAnswered + 1;
                        }
                    }
                }

            }
        }
        return qAll - qAnswered;
    }

    public interface OnItemClickListener {
        void onClick(Group Item);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView group_name;
        ImageView ic_notifications, ic_playlist_add_check;

        ViewHolder(View itemView) {
            super(itemView);
            group_name = itemView.findViewById(R.id.group_name);
            ic_notifications = itemView.findViewById(R.id.ic_notifications);
            ic_playlist_add_check = itemView.findViewById(R.id.ic_playlist_add_check);
        }

        void click(final Group group, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(group);
                }
            });
        }
    }
}

