package com.ivib.questionGroup;

import android.content.Context;

import com.ivib.utility.GlobalClass;

public class QuestionGroupPresenter {

    private final Context context;
    private final QuestionGroupView view;

    QuestionGroupPresenter(Context context, QuestionGroupView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت لیست گروه ها
     */
    public void getQuestionGroupList() {
        // دریافت لیست گروه ها از مدل عمومی
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        view.getQuestionGroupListSuccess(globalVariable.getGlobalQGL());
    }
}
