package com.ivib.questionGroup;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ivib.R;
import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.remote.UserCheckListOperations;
import com.ivib.main.MainActivity;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.questionsByGroupid.QuestionsByGroupidActivity;
import com.ivib.utility.ConvertClass;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionGroupActivity extends AppCompatActivity implements QuestionGroupView {

    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_CODE = 5;
    private static Animation shakeAnimation;
    public SessionManager session;
    public String parent = "";
    private Context context;
    private RecyclerView list;
    private LinearLayout retryLinearLayout, empty_LinearLayout;
    private GlobalClass GlobalVariable;
    private dbCheckListOperations DBCheckListOp;
    private dbQuestionsFileOperations DBQuestionsFileOp;
    private ProgressBar progress;
    private QuestionGroupPresenter presenter;
    private QuestionGroupView view;
    private LocationManager locationManager;
    private LinearLayout content_layout;
    private Activity mActivity;
    private  Button buttonAddNewCheckList ,buttonConfirmation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_group);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        Intent intent = getIntent();
        if (null != intent) {
            // در ابتدا تست شود
            // TODO: بردن این اطلاعات به مدل عمومی - چون وقتی وارد صفحه سوالات میشی و بعد بر میگردی این اطلاعات از بین میره و ما دیگه نمیدونیم از کجا اومدیم.
            // دریافت صفحه ای که از آن آمده
            parent = intent.getStringExtra("parent");
        }

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
        content_layout = findViewById(R.id.content_layout);

        context = getApplicationContext();
        GlobalVariable = (GlobalClass) context.getApplicationContext();
        session = new SessionManager(getApplicationContext());
        DBCheckListOp = new dbCheckListOperations(context);
        DBQuestionsFileOp = new dbQuestionsFileOperations(context);
        view = this;

        // آپدیت اطلاعات مدل عمومی در داخل پایگاه داده
        UpdateGlobalToCheckListDb();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        list = findViewById(R.id.list);
        buttonAddNewCheckList = findViewById(R.id.buttonAddNewCheckList);
        buttonConfirmation = findViewById(R.id.buttonConfirmation);


        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        progress = findViewById(R.id.progress);
        presenter = new QuestionGroupPresenter(this, this);
        presenter.getQuestionGroupList();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (parent.contentEquals("Main") ||parent.contentEquals("TempCheckList") ){
            buttonAddNewCheckList.setVisibility(View.VISIBLE);
            buttonConfirmation.setVisibility(View.GONE);
        }else {
            buttonAddNewCheckList.setVisibility(View.GONE);
            buttonConfirmation.setVisibility(View.VISIBLE);
        }


        // کلیک بر روی ثبت چک لیست جدید
        buttonAddNewCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ثبت چک لیست جدید
                AddNewCheckList();
            }
        });

        buttonConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // آپدیت اطلاعات مدل عمومی در داخل پایگاه داده
        UpdateGlobalToCheckListDb();

        // دریافت اطلاعات گروه به همراه آخرین تغییرات
        presenter.getQuestionGroupList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }
    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String text) {
        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));
    }

    @Override
    public void getQuestionGroupListSuccess(List<Group> groupList) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        // آداپتر گروه سوالات
        QuestionGroupAdapter adapter = new QuestionGroupAdapter(getApplicationContext(), groupList,
                new QuestionGroupAdapter.OnItemClickListener() {

                    /**
                     * کلیک بر روی هر گروه
                     * @param Item گروه
                     */
                    @Override
                    public void onClick(Group Item) {

                        // رفتن به صفحه سوالات همین گروه
                        Intent intent = new Intent(getApplicationContext(), QuestionsByGroupidActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("GroupId", Item.getId());
                        bundle.putString("GroupName", Item.getName());
                        intent.putExtras(bundle);
                        startActivityForResult(intent, REQUEST_CODE);
                    }
                });
        list.setAdapter(adapter);
    }

    @Override
    public void clearList() {
    }

    /**
     * آپدیت آخرین تغییرات مدل عمومی در پایگاه داده
     */
    private void UpdateGlobalToCheckListDb() {
        List<Question> model = GlobalVariable.getGlobalQL();
        byte[] globalData = ConvertClass.ToByteGlobalData(model);
        DBCheckListOp.UpdateByGlobalData(GlobalVariable.getBranchId(), globalData);
    }


    /**
     * ایجاد یک چک لیست جدید
     */
    private void AddNewCheckList() {
        //دریاف طول و عرض جغرافیایی
        getLocation();

        // نمایش لودینگ
        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("در حال ثبت چک لیست لطفا منتظر بمانید");
        progressDialog.show();

        boolean resultCheckAnswerAllQuestions = CheckAnswerAllQuestions();
        // بررسی پاسخ به همه سوالات
        if (resultCheckAnswerAllQuestions) {

            // فراخوانی سرویس افزودن ثبت چک لیست جدید
            final Call<String> createdRequest = new UserCheckListOperations().GetService().AddCheckList(GlobalVariable.getBranchId(), GlobalVariable.getLatitude(), GlobalVariable.getLongitude());
            createdRequest.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {

                        // آپدیت شناسه چک لیست ثبت شده در پایگاه داده
                        UpdateCheckListInfoIdDb(response.body());
                    } else {
                        view.showMessage("عدم برقراری ارتباط با سرور، لطفا دوباره سعی نمایید!");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable throwable) {
                    progressDialog.dismiss();
                    view.showMessage("عدم برقراری ارتباط با سرور، لطفا دوباره سعی نمایید!");
                }
            });
        } else {
            progressDialog.dismiss();
            view.showMessage("لطفا به تمامی سوالات پاسخ داده و مستندات آنها را ارسال نمایید.");
        }
    }

    /**
     * دریافت موقعیت مکانی حال حاضر کاربر
     */
    private void getLocation() {

        // بررسی مجوز استفاده از جی پی اس
        if (ActivityCompat.checkSelfPermission(QuestionGroupActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (QuestionGroupActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(QuestionGroupActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            try {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();

                if (location != null) {
                    globalVariable.setLatitude(location.getLatitude());
                    globalVariable.setLongitude(location.getLongitude());

                } else if (location1 != null) {
                    globalVariable.setLatitude(location1.getLatitude());
                    globalVariable.setLongitude(location1.getLongitude());

                } else if (location2 != null) {
                    globalVariable.setLatitude(location2.getLatitude());
                    globalVariable.setLongitude(location2.getLongitude());
                }
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * بررسی پاسخ به همه سوالات و مستندات مربوطه
     *
     * @return boolean وضعیت پاسخ دهی
     */
    private boolean CheckAnswerAllQuestions() {

        // آپدیت لیست سوالات مخفی شده
        GlobalVariable.updateGlobalCQL();

        // جستجو همه سوالات
        for (Question q : GlobalVariable.getGlobalQL()) {

            // اگر سوال مخفی نشده باشد
            if (!GlobalVariable.getGlobalCQL().contains(q.getId())) {

                // اگر سوال مقداری بوده و مقدار آن خالی باشد
                if (q.getAnswerValue() && q.getDefaultAnswerValue().isEmpty()) {
                    Log.e("CheckAnswerAl"," // اگر سوال مقداری بوده و مقدار آن خالی باشد");

                    return false;

                }

                // اگر سوال گزینه ای بوده و گزینه جواب خالی باشد
                if (!q.getAnswerValue() && q.getDefaultAnswerChoiseId().isEmpty()) {
                    Log.e("CheckAnswerAl"," " + q.toString());

                    Log.e("CheckAnswerAl"," اگر سوال گزینه ای بوده و گزینه جواب خالی باشدد");

                    return false;
                }

                // اگر سوال دارای مستندات اجباری باشد
                if (q.getQuestionDocumentStatus() == 1) {

                    // بررسی وجود مستندات اضافه شده به سوال در پایگاه داده
                    boolean isExistFiles = DBQuestionsFileOp.IsExist(GlobalVariable.getCheckListInfoId(), q.getId());
                    Log.e("CheckAnswerAl"," /بررسی وجود مستندات اضافه شده به سوال در پایگاه داده");


                    // اگر هیچ فایلی اضافه نشده باشد
                    if (!isExistFiles) {
                        Log.e("CheckAnswerAl"," گر هیچ فایلی اضافه نشده باشد");

                        return false;
                    }
                }
            }

        }
        return true;
    }

    /**
     * آپدیت شناسه چک لیست ثبت شده در پایگاه داده
     *
     * @param checkListId شناسه چک لیست
     */
    private void UpdateCheckListInfoIdDb(String checkListId) {
        DBCheckListOp.UpdateByCheckListId(GlobalVariable.getBranchId(), checkListId);
        GotoMain();
    }

    /**
     * رفتن به صفحه اصلی
     */
    private void GotoMain() {
        Intent i = new Intent(context, MainActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}
