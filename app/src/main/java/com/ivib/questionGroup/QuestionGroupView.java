package com.ivib.questionGroup;

import com.ivib.model.Group;

import java.util.List;

public interface QuestionGroupView {

    void showWait();

    void removeWait();

    void showEmpty();

    void hideEmpty();

    void retry();

    void hideRetry();

    void showMessage(String text);

    void getQuestionGroupListSuccess(List<Group> groupList);

    void clearList();
}
