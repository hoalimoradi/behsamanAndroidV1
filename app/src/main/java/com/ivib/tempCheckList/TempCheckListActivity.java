package com.ivib.tempCheckList;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.main.MainActivity;
import com.ivib.model.CheckList;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.questionGroup.QuestionGroupActivity;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class TempCheckListActivity extends AppCompatActivity implements TempCheckListView {

    private static Animation shakeAnimation;
    public SessionManager session;
    private RecyclerView list;
    private LinearLayout retryLinearLayout, empty_LinearLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progress;
    private TempCheckListPresenter presenter;
    private TempCheckListView view;
    private LinearLayout content_layout;
    private Activity mActivity;
    public  TempCheckListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_check_list);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
        content_layout = findViewById(R.id.content_layout);


        session = new SessionManager(getApplicationContext());
        TextView mTitle = findViewById(R.id.mtitle);
        mTitle.setText("چک لیست های موقت");

        list = findViewById(R.id.list);
        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        progress = findViewById(R.id.progress);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.material_red_200, R.color.material_red_300);
        swipeRefreshLayout.setBackgroundColor(Color.TRANSPARENT);
        view = this;

        presenter = new TempCheckListPresenter(this, view);

        // دریافت اطلاعات لیست چک لیست های موقت
        presenter.getCheckLists();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clearList();
                        presenter.getCheckLists();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String text) {
        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));
    }

    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    /**
     * پر کردن آداپتر لیست چک لیست های موقت
     *
     * @param checkLists لیست چک لیست های موقت
     */
    @Override
    public void getUserCheckList(final List<CheckList> checkLists) {

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        adapter = new TempCheckListAdapter(getApplicationContext(), checkLists,
                new TempCheckListAdapter.OnItemClickListener() {

                    /**
                     * کلیک بر روی دکمه ویرایش
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickEdit(View v, int position) {

                        // دریافت اطلاعات سوالات چک لیست موقت
                        List<Question> qListFromDB = presenter.getQListFromDB(Integer.parseInt(checkLists.get(position).getId()));

                        // دریافت گروه های چک لیست از روی سوالات
                        List<Group> groupList = new ArrayList<>();
                        List<String> sList = new ArrayList<>();
                        for (Question q : qListFromDB) {
                            Group group = new Group();
                            group.setId(q.getQuestionGroupId());
                            group.setName(q.getQuestionGroupName());

                            if (!sList.contains(q.getQuestionGroupId())) {
                                groupList.add(group);
                                sList.add(q.getQuestionGroupId());
                            }
                        }

                        // پر کردن لیست گروه ها و سوالات در کلاس عمومی
                        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
                        globalVariable.setGlobalQL(qListFromDB);
                        globalVariable.setGlobalQGL(groupList);
                        globalVariable.setBranchId(checkLists.get(position).getBranchId());
                        globalVariable.setCheckListInfoId(Integer.parseInt(checkLists.get(position).getId()));

                        //ست کردن کلاس والد جهت برگشت و رفتن به صفحه لیست گروه سوالات
                        Intent intent = new Intent(getApplicationContext(), QuestionGroupActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("parent", "TempCheckList");
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                    /**
                     * کلیک بر روی دکمه حذف
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickDelete(View v, final int position) {


                        Typeface font = Typeface.createFromAsset(getAssets(), "IRANSansMobile.ttf");
                        AlertDialog dialog = new AlertDialog.Builder(TempCheckListActivity.this)
                                .setMessage( "آیا از حذف این چک لیست مطمئن هستی؟")
                                .setCancelable(true)
                                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // حذف کل اطلاعات چک لیست موقت از پایگاه داده
                                        presenter.deleteCheckListInDb(Integer.parseInt(checkLists.get(position).getId()));



                                        removeItem(position);
                                        view.showMessage("چک لیست با موفقیت حذف شد!");

                                    }
                                })
                                .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();

                        TextView textView = dialog.findViewById(android.R.id.message);
                        if (textView != null) {
                            textView.setTypeface(font);
                        }
                        Button btn1 = dialog.findViewById(android.R.id.button1);
                        if (btn1 != null) {
                            btn1.setTypeface(font);
                        }
                        Button btn2 = dialog.findViewById(android.R.id.button2);
                        if (btn2 != null) {
                            btn2.setTypeface(font);
                        }



//                        swipeRefreshLayout.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                swipeRefreshLayout.setRefreshing(true);
//                            }
//                        });
                    }
                });

        list.setAdapter(adapter);
    }

    public void removeItem(int position){
        adapter.removeItem(position);
        if (adapter.getItemCount()==0)
        {
            showEmpty();
        }
    }

    @Override
    public void clearList() {

        List<CheckList> data = new ArrayList<>();
        TempCheckListAdapter adapter = new TempCheckListAdapter(getApplicationContext(), data,
                new TempCheckListAdapter.OnItemClickListener() {
                    @Override
                    public void onClickEdit(View v, int position) {
                    }

                    @Override
                    public void onClickDelete(View v, int position) {
                    }
                });
        list.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}
