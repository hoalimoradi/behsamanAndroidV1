package com.ivib.tempCheckList;

import android.content.Context;

import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.model.CheckList;
import com.ivib.model.Question.Question;
import com.ivib.utility.ConvertClass;

import java.util.ArrayList;
import java.util.List;

public class TempCheckListPresenter {

    private final Context context;
    private final TempCheckListView view;

    TempCheckListPresenter(Context context, TempCheckListView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت اطلاعات چک لیست موقت از پایگاه داده
     */
    public void getCheckLists() {
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        List<CheckListInfo> checkListInfo = dbCheckListOp.GetAllIfNull();

        List<CheckList> model = new ArrayList<>();
        for (CheckListInfo item :
                checkListInfo) {
            CheckList mItem = new CheckList(item.Id + "", item.BranchId, item.CreateDateTime, item.BranchArea, item.BranchesManagement, item.BranchDomainName, item.BranchName, item.BranchId, 0, 0);
            model.add(mItem);
        }
        if (model.isEmpty()){
            view.showEmpty();

            view.hideRetry();
            view.removeWait();

        }else {
            view.hideEmpty();
            view.hideRetry();
            view.removeWait();
            view.getUserCheckList(model);
        }

    }

    /**
     * دریافت اطلاعات سوالات یک چک لیست موقت از پایگاه داده
     *
     * @param checkListInfoId شناسه جدول چک لیست
     * @return List<Question> لیستی از سوالات
     */
    public List<Question> getQListFromDB(int checkListInfoId) {
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        CheckListInfo checkListInfo = dbCheckListOp.GetByBranchId(checkListInfoId);
        if (checkListInfo != null){
            return ConvertClass.ToModelGlobalData(checkListInfo.GlobalData);
        }
        return null;
    }

    /**
     * حذف یک چک لیست موقت از پایگاه داده
     *
     * @param checkListInfoId شناسه جدول چک لیست موقت
     */
    public void deleteCheckListInDb(int checkListInfoId) {

        // حذف چک لیست از پایگاه داده
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        dbCheckListOp.Delete(checkListInfoId);

        // حذف سوالات چک لیست از پایگاه داده
        dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
        dbQuestionOp.DeleteAll(checkListInfoId);

        // حذف فایل های سوالات چک لیست از پایگاه داده
        dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
        dbQuestionsFileOp.DeleteAll(checkListInfoId);
    }
}
