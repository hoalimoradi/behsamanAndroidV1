package com.ivib.tempCheckList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ivib.R;
import com.ivib.model.CheckList;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.FontChangeCrawler;

import java.util.List;

public class TempCheckListAdapter extends RecyclerView.Adapter<TempCheckListAdapter.ViewHolder> {

    private final TempCheckListAdapter.OnItemClickListener listener;
    private List<CheckList> data;
    private Context context;

    TempCheckListAdapter(Context context, List<CheckList> data, TempCheckListAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public TempCheckListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_temp_check_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new TempCheckListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TempCheckListAdapter.ViewHolder holder, int position) {
        holder.BranchesManagment.setText(data.get(position).getBranchesManagment());
        holder.DomainName.setText(data.get(position).getBranchDomainName());
        holder.BranchName.setText(data.get(position).getBranchName());
        holder.Area.setText(data.get(position).getBranchArea());
        holder.CreateDateTime.setText(data.get(position).getCreateDateTime());
    }

    @Override
    public int getItemCount() {

        return  data == null ? 0 : data.size();

    }


    public void updateData(List<CheckList> checkLists) {
        data.clear();
        data.addAll(checkLists);
        notifyDataSetChanged();
    }
    public void addItem(int position, CheckList checkList) {
        data.add(position, checkList);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnItemClickListener {
        void onClickEdit(View v, int position);

        void onClickDelete(View v, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView BranchesManagment;
        AutofitTextView DomainName;
        AutofitTextView BranchName;
        AutofitTextView Area;
        AutofitTextView CreateDateTime;

        LinearLayout deleteLinearLayout, editLinearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            BranchesManagment = itemView.findViewById(R.id.BranchesManagment);
            DomainName = itemView.findViewById(R.id.DomainName);
            BranchName = itemView.findViewById(R.id.BranchName);
            Area = itemView.findViewById(R.id.Area);
            CreateDateTime = itemView.findViewById(R.id.CreateDateTime);
            deleteLinearLayout = itemView.findViewById(R.id.deleteLinearLayout);
            editLinearLayout = itemView.findViewById(R.id.editLinearLayout);

            deleteLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickDelete(v, getAdapterPosition());
                }
            });

            editLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickEdit(v, getAdapterPosition());
                }
            });
        }
    }
}

