package com.ivib.InputUser;

import android.content.Context;
import android.util.Log;

import com.ivib.model.UserProfile.Branch;
import com.ivib.utility.GlobalClass;

import java.util.ArrayList;
import java.util.List;

public class InputUserPresenter {
    private final Context context;
    private final InputUserView view;
    private List<Branch> branchList;
    private List<Branch> temp = new ArrayList<>();
    InputUserPresenter(Context context, InputUserView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت لیست شعب
     */
    public void getBranchList() {
        view.clearList();
        // دریافت لیست شعب از مدل عمومی
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        view.showBranchList(globalVariable.getBranchList());
    }

    public void getBranchList(String text) {
        // دریافت لیست شعب از مدل عمومی
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();

        branchList = globalVariable.getBranchList();

        temp.clear();
        for(Branch branch : branchList) {
            if (branch.getArea().contains(text)

                    ||branch.getBranchName() .contains(text)
                    ||branch.getDomainName().contains(text)){
                temp.add(branch);
                Log.e("temp ",branch.getAddress());
            }
        }
        Log.e("temp ", String.valueOf(temp.size()));
        view.clearList();
        view.showBranchList(temp);
    }
}

