package com.ivib.InputUser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ivib.InputOutputUserMap.InputUserMapActivity;
import com.ivib.R;
import com.ivib.data.remote.InputOutputUserOperations;
import com.ivib.model.UserProfile.Branch;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputUserActivity extends AppCompatActivity implements InputUserView {

    private static Animation shakeAnimation;
    public String branchId;
    public SessionManager session;
    private InputUserPresenter presenter;
    private RecyclerView list;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout retryLinearLayout, empty_LinearLayout, content_layout;
    private ProgressBar progress;
    private Activity mActivity;

    SearchView editsearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_output);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        session = new SessionManager(getApplicationContext());
        InputUserView inputUserView = this;

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
        editsearch = (SearchView) findViewById(R.id.search);

        content_layout = findViewById(R.id.content_layout);
        list = findViewById(R.id.list);
        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.material_red_200, R.color.material_red_300);
        swipeRefreshLayout.setBackgroundColor(Color.TRANSPARENT);

        presenter = new InputUserPresenter(this, inputUserView);

        // دریافت اطلاعات شعب کاربر
        presenter.getBranchList();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clearList();
                        presenter.getBranchList();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });


        editsearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("query ",query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                Log.e("newText ",newText);
                if (newText.length()>3){
                    presenter.getBranchList(newText);

                }

                return false;
            }
        });


        editsearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.e("setOnCloseListener","  onClose  onClose  ");
                presenter.getBranchList();
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void clearList() {
        List<Branch> data = new ArrayList<Branch>();
        InputUserAdapter adapter = new InputUserAdapter(getApplicationContext(), data,
                new InputUserAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(int position) {
                    }
                });
        list.setAdapter(adapter);
    }

    @Override
    public void showMessage(String text) {
        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));
    }

    /**
     * نمایش لیست شعب
     *
     * @param branchList مدل لیست شعب
     */
    @Override
    public void showBranchList(final List<Branch> branchList) {

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        // آداپتر لیست شعب
        InputUserAdapter adapter = new InputUserAdapter(getApplicationContext(), branchList,
                new InputUserAdapter.OnItemClickListener() {

                    /**
                     * کلیک بر روی شعبه انتخابی
                     * @param position موقعیت
                     */
                    @Override
                    public void onClick(int position) {

                        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();

                        // عدم دریافت موقعیت مکانی از جی پی اس
                        if (globalVariable.getLatitude() == 0.0) {
                            showMessage("برای مشخص کردن محل خود، از نقشه استفاده کنید.");

                            // رفتن به صفحه نمایش نقشه
                            Intent intent = new Intent(getApplicationContext(), InputUserMapActivity.class);
                            startActivity(intent);
                        } else {

                            branchId = branchList.get(position).getId();
                            final String branchName = branchList.get(position).getBranchName();

                            // فراخوانی سرویس ثبت ورود به شعبه انتخاب شده به همراه موقعیت مکانی
                            final Call<Void> createdRequest = new InputOutputUserOperations().GetService().RegisterEntry(branchId, globalVariable.getLatitude(), globalVariable.getLongitude());
                            createdRequest.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    if (response.isSuccessful()) {
                                        showMessage("شما به شعبه " + branchName + " وارد شدید.");

                                        // آپدیت کد شعبه در مدل عمومی
                                        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
                                        globalVariable.setBranchId(branchId);
                                        finish();
                                    } else {
                                        showMessage("مشکل در ورود به شعبه! لطفا دوباره سعی نمایید.");
                                    }
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable throwable) {
                                }
                            });
                        }
                    }
                });
        list.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}
