package com.ivib.InputUser;

import com.ivib.model.UserProfile.Branch;

import java.util.List;

public interface InputUserView {

    void showWait();

    void removeWait();

    void showEmpty();

    void hideEmpty();

    void retry();

    void hideRetry();

    void clearList();

    void showMessage(String text);

    void showBranchList(List<Branch> branchList);
}
