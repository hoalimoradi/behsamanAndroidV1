package com.ivib.InputUser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebanx.swipebtn.OnActiveListener;
import com.ebanx.swipebtn.SwipeButton;
import com.ivib.R;
import com.ivib.model.UserProfile.Branch;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.FontChangeCrawler;

import java.util.List;
import java.util.Locale;

public class InputUserAdapter extends RecyclerView.Adapter<InputUserAdapter.ViewHolder> {

    private final InputUserAdapter.OnItemClickListener listener;
    private List<Branch> data;
    private List<Branch> tempList = null;
    private Context context;

    InputUserAdapter(Context context, List<Branch> data, InputUserAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public InputUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_input_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new InputUserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InputUserAdapter.ViewHolder holder, int position) {
        holder.BranchesManagment.setText(data.get(position).getBranchesManagment());
        holder.DomainName.setText(data.get(position).getDomainName());
        holder.BranchName.setText(data.get(position).getBranchName());
        holder.Address.setText(data.get(position).getAddress());
        holder.PostalCode.setText(data.get(position).getPostalCode());
    }

    @Override
    public int getItemCount() {
        return  data == null ? 0 : data.size();

    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView BranchesManagment;
        AutofitTextView DomainName;
        AutofitTextView BranchName;
        AutofitTextView Address;
        AutofitTextView PostalCode;
        AutofitTextView register_entry;


        ViewHolder(final View itemView) {
            super(itemView);
            BranchesManagment = itemView.findViewById(R.id.BranchesManagment);
            DomainName = itemView.findViewById(R.id.DomainName);
            BranchName = itemView.findViewById(R.id.BranchName);
            Address = itemView.findViewById(R.id.Address);
            PostalCode = itemView.findViewById(R.id.PostalCode);
            register_entry = itemView.findViewById(R.id.register_entry);
            register_entry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // ثبت  کاربر
                    listener.onClick(getAdapterPosition());
                }
            });

        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        tempList.clear();
        if (charText.length() == 0) {
            tempList.addAll(data);
        } else {
            for (Branch wp : data) {
                if (wp.getBranchName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    tempList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}

