package com.ivib.utility;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ivib.R;


/**
 * Created by H.Alimoradi on 1/4/2017.
 */
public class CustomToast {

    // Custom Toast Method
    public void Show_Toast(Context context, View view, String error) {

        try{
            // Layout Inflater for inflating custom view
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // inflate the layout over view
            View layout = inflater.inflate(R.layout.custom_toast,
                    (ViewGroup) view.findViewById(R.id.toast_root));

            // Get TextView id and set error
            TextView text = (TextView) layout.findViewById(R.id.toast_error);
            text.setText(error);

            FontChangeCrawler fontChanger = new FontChangeCrawler(layout.getContext().getAssets(), "IRANSansMobile.ttf");
            fontChanger.replaceFonts((ViewGroup)layout);

            Toast toast = new Toast(context);// Get Toast Context
            toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);// Set
            // Toast
            // gravity
            // and
            // Fill
            // Horizoontal

            toast.setDuration(Toast.LENGTH_LONG);// Set Duration
            toast.setView(layout); // Set Custom View over toast


            toast.show();// Finally show toast
        }
        catch (Exception e)
        {
            Log.e("Exception" ,"Show_Toast" +e.toString());
        }

    }
}
