package com.ivib.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ivib.model.Question.Question;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ConvertClass {
    private static byte[] toByteArray(Object obj) throws IOException {
        byte[] bytes;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
        return bytes;
    }

    private static Object toObject(byte[] bytes) throws IOException, ClassNotFoundException {
        Object obj;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bis = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        }
        finally {
            if (bis != null) {
                bis.close();
            }
            if (ois != null) {
                ois.close();
            }
        }
        return obj;
    }


    public static String toString(byte[] bytes) {
        return new String(bytes);
    }

    public static byte[] ToByteGlobalData(List<Question> input) {

        try {
            return ConvertClass.toByteArray(input.toArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Question> ToModelGlobalData(byte[] input) {
        try {
            Object obj1 = ConvertClass.toObject(input);
            Gson gsonBuilder = new GsonBuilder().create();
            String jsonFromObj = gsonBuilder.toJson(obj1);
            Type collectionType = new TypeToken<List<Question>>(){}.getType();
            List<Question> list = gsonBuilder.fromJson(jsonFromObj,collectionType );
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

