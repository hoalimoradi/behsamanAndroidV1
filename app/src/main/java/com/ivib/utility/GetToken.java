package com.ivib.utility;

import android.content.Context;

import com.ivib.data.remote.LoginOperations;
import com.ivib.model.Token;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ho on 6/16/2018 AD.
 */

public class GetToken {

    public GetToken() {

    }

    private SessionManager session;
    public void getToken(final Context context,String username,String password){
        final Call<Token> createdRequest= new LoginOperations().GetService().GetAccessToken("grant_type",username,password);
        createdRequest.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {

                    if (response.body().equals(null)) {


                    }else {
                        GlobalClass globalVariable  = (GlobalClass)context.getApplicationContext() ;

                        session = new SessionManager(context);
                        session.updateLoginSession(response.body().getAccess_token(),response.body().getExpires_in() );

                    }

                } else if (response.code() == 401) {




                } else {

                  }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable throwable) {

            }
        });

    }
}
