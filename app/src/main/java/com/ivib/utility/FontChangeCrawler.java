package com.ivib.utility;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Help :
 *
 * Replace Entire Activity's Typeface :
 *  FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "font.otf");
    fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

 *sdapter :
 * convertView = inflater.inflate(R.layout.listitem, null);
    fontChanger.replaceFonts((ViewGroup)convertView);
 */

public class FontChangeCrawler {
    private Typeface typeface;

    public FontChangeCrawler(Typeface typeface)
    {

        this.typeface = typeface;
    }

    public FontChangeCrawler(AssetManager assets, String assetsFontFileName)
    {
        typeface = Typeface.createFromAsset(assets, assetsFontFileName);
    }

    public void replaceFonts(ViewGroup viewTree)
    {
        View child;
        for(int i = 0; i < viewTree.getChildCount(); ++i)
        {
            child = viewTree.getChildAt(i);
            if(child instanceof ViewGroup)
            {
                // recursive call
                replaceFonts((ViewGroup)child);
            }
            else if(child instanceof TextView)
            {
                // base case
                ((TextView) child).setTypeface(typeface);
            }
        }
    }
}
