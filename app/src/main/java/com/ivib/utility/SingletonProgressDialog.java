package com.ivib.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;

import com.ivib.R;


public class SingletonProgressDialog {
    private static ProgressDialog mProgressDialog = null;

     public static void Start(Context context) {
        Stop(context);
        try {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("لطفا منتظر بمانید ...");
            mProgressDialog.show();

        } catch (Exception e){
            Log.e("",e.toString());
        }

    }

    public static void Start(Context context, String message) {
        Stop(context);
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public static void Start(Context context, String message,int i) {
        Stop(context);
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
        if (i==0)
            mProgressDialog.getWindow().setGravity(Gravity.BOTTOM);

    }

    public static void Stop(Context context) {
        dismissProgressDialog(mProgressDialog,context);
    }

    /**
     * Dismiss {@link ProgressDialog} with check for nullability and SDK version
     *
     * @param dialog instance of {@link ProgressDialog} to dismiss
     */
    private static void dismissProgressDialog(ProgressDialog dialog, Context context) {
        if (dialog != null && dialog.isShowing()) {

            // if the Context used here was an activity AND it hasn't been finished or destroyed
            // then dismiss it
            if (context instanceof Activity) {

                // Api >=17
                if (!((Activity) context).isFinishing() ){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        if (!((Activity) context).isDestroyed()) {
                            dismissWithExceptionHandling(dialog);
                        }
                    } else {
                        // Api < 17. Unfortunately cannot check for isDestroyed()
                        dismissWithExceptionHandling(dialog);
                    }
                }
            } else
                // if the Context used wasn't an Activity, then dismiss it too
                dismissWithExceptionHandling(dialog);
        }
        dialog = null;
    }


    /**
     * Dismiss {@link ProgressDialog} with try catch
     *
     * @param dialog instance of {@link ProgressDialog} to dismiss
     */
    private static void dismissWithExceptionHandling(ProgressDialog dialog) {
        try {
            dialog.dismiss();
        } catch (final IllegalArgumentException e) {
            // Do nothing.
        } catch (final Exception e) {
            // Do nothing.
        } finally {
            dialog = null;
        }
    }
}
