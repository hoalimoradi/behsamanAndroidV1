package com.ivib.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.ivib.R;
import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.ivib.data.local.Entity.TokenInfo;
import com.ivib.data.local.Entity.UserInfo;
import com.ivib.data.local.Entity.UserPermissionsInfo;
import com.ivib.data.remote.LoginOperations;
import com.ivib.data.remote.UserProfileOperations;
import com.ivib.login.LoginActivity;
import com.ivib.model.Token;
import com.ivib.model.UserProfile.Permission;
import com.ivib.model.UserProfile.UserProfile;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingletonAppBase {

    private static Context mContext;
    private static String instanceAccessToken = "";
    private static TokenInfo instanceTokenInfo = new TokenInfo();
    private static UserInfo instanceUserInfo = new UserInfo();
    private static List<UserPermissionsInfo> instanceUserPermissionsInfo = null;

    @Nullable
    public static Void CheckBaseInfo(Activity activity) {
        mContext = activity;

        if (!new CheckConnection().isOnline(activity)) {
            GotoIsOffline(activity);
        } else if (instanceAccessToken == null ||
                instanceTokenInfo == null ||
                instanceTokenInfo.AccessToken == null ||
                instanceUserInfo == null ||
                instanceUserInfo.Id == null ||
                instanceUserPermissionsInfo == null) {
            try {
                DBHelper dbHelper = new DBHelper(mContext);

                // get TokenInfo from SQLLite
                Dao<TokenInfo, Integer> tokenAppDao = dbHelper.getTokenInfoDao();
                QueryBuilder<TokenInfo, Integer> builderTokenApp = tokenAppDao.queryBuilder();
                builderTokenApp.limit(1);
                builderTokenApp.orderBy("TokenAppId", false);
                TokenInfo savedTokenInfo = tokenAppDao.queryForFirst(builderTokenApp.prepare());

                // get UserInfo from SQLLite
                Dao<UserInfo, Integer> userInfoDao = dbHelper.getUserInfoDao();
                QueryBuilder<UserInfo, Integer> builderUserInfo = userInfoDao.queryBuilder();
                builderUserInfo.limit(1);
                builderUserInfo.orderBy("Id", false);
                UserInfo savedUserInfo = userInfoDao.queryForFirst(builderUserInfo.prepare());

                // get UserPermissionsInfo from SQLLite
                Dao<UserPermissionsInfo, Integer> userPermissionsInfoDao = dbHelper.getUserPermissionsInfoDao();
                List<UserPermissionsInfo> savedUserPermissionsInfo = userPermissionsInfoDao.queryForAll();

                if (savedTokenInfo == null ||
                        savedUserInfo == null ||
                        savedUserPermissionsInfo == null) {
                    GotoLogin();
                } else {
                    LoginUser(savedTokenInfo);
                }
            } catch (SQLException e) {
                GotoLogin();
            }
        }
        return null;
    }

    public static Void Clear(Activity activity) {
        mContext = activity;

        try {
            setAccessToken("");
            setTokenInfo(null);
            setUserInfo(null);
            setUserPermissionsInfo(null);

            DBHelper dbHelper = new DBHelper(mContext);

            // Delete TokenInfo from SQLLite
            Dao<TokenInfo, Integer> tokenAppDao = dbHelper.getTokenInfoDao();
            DeleteBuilder<TokenInfo, Integer> builderTokenApp = tokenAppDao.deleteBuilder();
            builderTokenApp.delete();

            // Delete UserInfo from SQLLite
            Dao<UserInfo, Integer> userInfoDao = dbHelper.getUserInfoDao();
            DeleteBuilder<UserInfo, Integer> builderUserInfo = userInfoDao.deleteBuilder();
            builderUserInfo.delete();

            // Delete UserPermissionsInfo from SQLLite
            Dao<UserPermissionsInfo, Integer> userPermissionsInfoDao = dbHelper.getUserPermissionsInfoDao();
            DeleteBuilder<UserPermissionsInfo, Integer> builderPermissionsInfo = userPermissionsInfoDao.deleteBuilder();
            builderPermissionsInfo.delete();

            // حذف چک لیست از پایگاه داده
            dbCheckListOperations dbCheckListOp = new dbCheckListOperations(mContext);
            dbCheckListOp.DeleteAll();

            // حذف سوالات چک لیست از پایگاه داده
            dbQuestionOperations dbQuestionOp = new dbQuestionOperations(mContext);
            dbQuestionOp.DeleteAll();

            // حذف فایل های سوالات چک لیست از پایگاه داده
            dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(mContext);
            List<QuestionsFilesInfo> questionsFilesInfos = dbQuestionsFileOp.GetAll();
            for (QuestionsFilesInfo item : questionsFilesInfos) {
                File file = new File(item.FileAddress);
                boolean deleted = file.delete();
            }
            dbQuestionsFileOp.DeleteAll();

        } catch (SQLException ignored) {

        }
        return null;
    }

    public static void ResetToken(Activity activity) {
        mContext = activity;

        if (!new CheckConnection().isOnline(activity)) {
            GotoIsOffline(activity);
        } else {
            try {
                DBHelper dbHelper = new DBHelper(mContext);
                Dao<TokenInfo, Integer> tokenAppDao = dbHelper.getTokenInfoDao();
                QueryBuilder<TokenInfo, Integer> builder = tokenAppDao.queryBuilder();
                builder.limit(1);
                builder.orderBy("TokenAppId", false);
                TokenInfo savedTokenInfo = tokenAppDao.queryForFirst(builder.prepare());

                LoginUser(savedTokenInfo);

            } catch (SQLException e) {
                GotoLogin();
            }
        }
    }

    public static String getAccessToken() {
        return instanceAccessToken;
    }

    public static void setAccessToken(String instanceAccessToken) {
        SingletonAppBase.instanceAccessToken = instanceAccessToken;
    }

    public static TokenInfo getTokenInfo(Activity activity) {
        DBHelper dbHelper = new DBHelper(activity);
        Dao<TokenInfo, Integer> tokenAppDao = null;
        try {
            tokenAppDao = dbHelper.getTokenInfoDao();
            QueryBuilder<TokenInfo, Integer> builderTokenApp = tokenAppDao.queryBuilder();
            builderTokenApp.limit(1);
            builderTokenApp.orderBy("TokenAppId", false);
            TokenInfo savedTokenInfo = tokenAppDao.queryForFirst(builderTokenApp.prepare());
            if (savedTokenInfo == null)
                return null;

            instanceTokenInfo = savedTokenInfo;
            instanceAccessToken = savedTokenInfo.AccessToken;
            return instanceTokenInfo;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setTokenInfo(TokenInfo instanceTokenInfo) {
        SingletonAppBase.instanceTokenInfo = instanceTokenInfo;
    }

    public static UserInfo getUserInfo() {
        return instanceUserInfo;
    }

    public static void setUserInfo(UserInfo instanceUserInfo) {
        SingletonAppBase.instanceUserInfo = instanceUserInfo;
    }

    public static List<UserPermissionsInfo> getUserPermissionsInfo() {
        return instanceUserPermissionsInfo;
    }

    public static void setUserPermissionsInfo(List<UserPermissionsInfo> instanceUserPermissionsInfo) {
        SingletonAppBase.instanceUserPermissionsInfo = instanceUserPermissionsInfo;
    }

    private static void GotoLogin() {
        SingletonProgressDialog.Stop(mContext);
        Intent intent = new Intent(mContext, LoginActivity.class);
        mContext.startActivity(intent);
    }

    private static void GotoIsOffline(Activity activity) {
        Toast.makeText(mContext, R.string.message_internet_is_not_available, Toast.LENGTH_LONG).show();
    }

    private static void LoginUser(final TokenInfo savedTokenInfo) {
        setAccessToken("");
        setTokenInfo(null);
        setUserInfo(null);
        setUserPermissionsInfo(null);

        SingletonProgressDialog.Start(mContext);
        final Call<Token> createdRequest = new LoginOperations().GetService()
                .GetAccessToken("password", savedTokenInfo.getUserName(), savedTokenInfo.getPassword());
        createdRequest.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {

                    if (GetTokenInfo(response.body(), savedTokenInfo.getPassword())) {
                        GetUserInfo();
                    }
                } else {
                    GotoLogin();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable throwable) {
                GotoLogin();
            }
        });
    }

    private static boolean GetTokenInfo(Token jsonObject, String password) {
        TokenInfo tokenInfo = new TokenInfo(0, jsonObject.access_token,
                jsonObject.token_type, Integer.parseInt(jsonObject.expires_in),
                jsonObject.userName, password, jsonObject.issued,
                jsonObject.expires);

        try {
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<TokenInfo, Integer> tokenAppDao = dbHelper.getTokenInfoDao();
            DeleteBuilder<TokenInfo, Integer> deleteBuilder = tokenAppDao.deleteBuilder();
            deleteBuilder.delete();
            QueryBuilder<TokenInfo, Integer> builder = tokenAppDao.queryBuilder();
            builder.limit(1);
            builder.orderBy("TokenAppId", false);

            int result = tokenAppDao.create(tokenInfo);
            if (result == 1) {
                instanceTokenInfo = tokenAppDao.queryForFirst(builder.prepare());
                instanceAccessToken = instanceTokenInfo.AccessToken;
            }

        } catch (SQLException e) {
            GotoLogin();
            return false;
        }

        return true;
    }

    private static void GetUserInfo() {
        final Call<UserProfile> createdRequest = new UserProfileOperations().GetService().GetUserProfile();
        createdRequest.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.isSuccessful()) {
                    FillSingletonUserInfo(response.body());
                    FillSingletonPermissionInfo(response.body().getPermissions());
                    SingletonProgressDialog.Stop(mContext);
                } else {
                    ShowExceptionMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable throwable) {
                ShowExceptionMessage(null);
            }
        });
    }

    private static void FillSingletonUserInfo(UserProfile userProfile) {
        try {
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<UserInfo, Integer> userInfoDao = dbHelper.getUserInfoDao();
            DeleteBuilder<UserInfo, Integer> deleteBuilder = userInfoDao.deleteBuilder();
            deleteBuilder.delete();
            UserInfo userInfoViewModel = new UserInfo();
            userInfoViewModel.setId(userProfile.getId());
            userInfoViewModel.setFirstName(userProfile.getFirstName());
            userInfoViewModel.setLastName(userProfile.getLastName());
            userInfoViewModel.setUserName(userProfile.getUserName());
            userInfoViewModel.setImageLastAddress(userProfile.getImageLastAddress());
            userInfoViewModel.setNewNotificationCount(userProfile.getNewNotificationCount());

            userInfoDao.create(userInfoViewModel);
            UserInfo getUserInfo = userInfoDao.queryBuilder().where().eq("Id", userProfile.getId()).queryForFirst();

            setUserInfo(getUserInfo);
        } catch (Exception e) {
            Toast.makeText(mContext, R.string.message_reinstall_app, Toast.LENGTH_LONG).show();
        }
    }

    private static void FillSingletonPermissionInfo(List<Permission> permissions) {
        try {
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<UserPermissionsInfo, Integer> userPermissionsInfoDao = dbHelper.getUserPermissionsInfoDao();
            DeleteBuilder<UserPermissionsInfo, Integer> deleteBuilder = userPermissionsInfoDao.deleteBuilder();
            deleteBuilder.delete();

            for (Permission item : permissions) {
                UserPermissionsInfo userPermissionsInfoModel = new UserPermissionsInfo();
                userPermissionsInfoModel.setPermissionName(item.getValue());
                userPermissionsInfoDao.create(userPermissionsInfoModel);
            }

            List<UserPermissionsInfo> getUserPermissionsInfo = userPermissionsInfoDao.queryForAll();
            setUserPermissionsInfo(getUserPermissionsInfo);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void ShowExceptionMessage(Integer errorCode) {
        SingletonProgressDialog.Stop(mContext);
        Toast.makeText(mContext, R.string.message_error_connect_server, Toast.LENGTH_LONG).show();
        if (errorCode != null) {
            Toast.makeText(mContext, "Server Error Code: " + errorCode, Toast.LENGTH_LONG).show();
        }
    }
}
