package com.ivib.utility;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.ivib.model.Group;
import com.ivib.model.LatestStatusResponse;
import com.ivib.model.Question.AnswerList;
import com.ivib.model.Question.ConditionalQuestions;
import com.ivib.model.Question.Question;
import com.ivib.model.UserProfile.Branch;

import java.util.ArrayList;
import java.util.List;

public class GlobalClass extends Application {


    public LatestStatusResponse latestStatusResponse;
    public boolean tapAnimation = false;
    private List<Group> globalQGL;
    private List<Question> globalQL;
    private List<String> globalCQL; // لیست ایدی سوالات مخفی
    private List<Branch> branchList;
    private String BranchId;
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private int CheckListInfoId;
    private boolean splash = true;
    private boolean mainPageConnectToServer = false;

    public int getCheckListInfoId() {
        return CheckListInfoId;
    }

    public void setCheckListInfoId(int checkListInfoId) {
        CheckListInfoId = checkListInfoId;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
    }


    public List<Group> getGlobalQGL() {
        return globalQGL;
    }

    public void setGlobalQGL(List<Group> globalQGL) {
        this.globalQGL = globalQGL;
    }

    public List<Question> getGlobalQL() {
        return globalQL;
    }

    public void setGlobalQL(List<Question> globalQL) {
        this.globalQL = globalQL;
    }

    public void setQuestionDefaultAnswer(String questionId, String defaultAnswerValue, String defaultChoiceId) {
        for (Question q : this.globalQL) {
            if (q.getId().equals(questionId)) {
                q.setDefaultAnswerChoiseId(defaultChoiceId);
                q.setDefaultAnswerValue(defaultAnswerValue);
            }
        }
    }

    /**
     * آپدیت لیست سوالات مخفی شده
     */
    public void updateGlobalCQL() {
        // خالی کردن لیست سوالات مخفی شده
        this.globalCQL = new ArrayList<>();

        // جستجو برای هر سوال از مدل عمومی
        for (Question q : this.globalQL) {

            // اگر سوال گزینه ای بوده و برای آن پاسخی وجود داشته باشد
            if (!q.getAnswerValue() && !q.getDefaultAnswerChoiseId().isEmpty()) {

                // جوستجو برای تمام پاسخ های این سوال
                for (AnswerList a : q.getAnswerList()) {

                    // یافتن پاسخ داده شده سوال
                    if (a.getId().equals(q.getDefaultAnswerChoiseId())) {

                        // اگر پاسخ سوال دارای شرایط مخفی کردن یک یا چند سوال باشد
                        if (a.getConditionalQuestions() != null && a.getConditionalQuestions().size() != 0){

                            // جوستجو بر روی همه شرایط یک جواب یک سوال
                            for (ConditionalQuestions cq : a.getConditionalQuestions()) {

                                // افزودن شناسه سوال مخفی شده در لیست
                                this.globalCQL.add(cq.getQuestionId());
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    public List<String> getGlobalCQL() {
        return globalCQL;
    }

    public void setGlobalCQL(List<String> globalCQL) {
        this.globalCQL = globalCQL;
    }

    public boolean isSplash() {
        return splash;
    }

    public void setSplash(boolean splash) {
        this.splash = splash;
    }

    public boolean isMainPageConnectToServer() {
        return mainPageConnectToServer;
    }

    public void setMainPageConnectToServer(boolean mainPageConnectToServer) {
        this.mainPageConnectToServer = mainPageConnectToServer;
    }

    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }

    public LatestStatusResponse getLatestStatusResponse() {
        return latestStatusResponse;
    }

    public void setLatestStatusResponse(LatestStatusResponse latestStatusResponse) {
        this.latestStatusResponse = latestStatusResponse;
    }


    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public boolean isTapAnimation() {
        return tapAnimation;
    }

    public void setTapAnimation(boolean tapAnimation) {
        this.tapAnimation = tapAnimation;
    }
}