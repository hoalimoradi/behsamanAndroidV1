package com.ivib.utility;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;


import com.ivib.data.remote.LoginOperations;
import com.ivib.login.LoginActivity;
import com.ivib.model.Token;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by H.Alimoradi on 7/29/2017.
 */
public class SessionManager {

    SharedPreferences pref;


    SharedPreferences.Editor editor;


    Context _context;

    int PRIVATE_MODE = 0;


    private static final String PREF_NAME = "behSazanPref";

    private static final String IS_First = "IS_First";

    private static final String IS_LOGIN = "IsLoggedIn";


    public String KEY_USERNAME = "username";
    public String KEY_PASS = "password";
    public String KEY_ACCESS_TOKEN= "access_token";
    public String KEY_expires_in= "KEY_expires_in";



    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String username,String password, String access_token , String expires_in){
        // Storing login value as TRUE

        editor.putBoolean(IS_LOGIN, false);

        editor.putBoolean(IS_LOGIN, true);


        editor.putString(KEY_USERNAME, username);


        editor.putString(KEY_PASS, password);


        editor.putString(KEY_ACCESS_TOKEN, access_token);

        editor.putString(KEY_expires_in, expires_in);



        // commit changes
        editor.commit();
    }
    public void updateUserId( String userId){

        editor.putString(KEY_USERNAME, userId);
        editor.commit();
    }

    public void updateLoginSession( String access_token , String expires_in){

        editor.putString(KEY_ACCESS_TOKEN, access_token);

        editor.putString(KEY_expires_in, expires_in);


        editor.commit();
    }


    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        // user pass id
        user.put(KEY_PASS, pref.getString(KEY_PASS, null));
        // user username id
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        //access token
        user.put(KEY_ACCESS_TOKEN, pref.getString(KEY_ACCESS_TOKEN, null));


        user.put(KEY_expires_in, pref.getString(KEY_expires_in, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }


    // Get IS_First State
    public boolean IS_First(){
        return pref.getBoolean(IS_First, true);
    }

}
