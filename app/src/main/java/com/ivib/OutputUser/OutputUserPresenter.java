package com.ivib.OutputUser;

import android.content.Context;

import com.ivib.data.remote.InputOutputUserOperations;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutputUserPresenter {
    private final Context context;
    private final OutputUserView view;

    OutputUserPresenter(Context context, OutputUserView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * ثبت خروج کاربر
     *
     * @param IdLatestStatusResponse شناسه آخرین وضعیت ورود کاربر
     * @param LatitudeOutput         موقعیت مکانی
     * @param LongitudeOutput        موقعیت مکانی
     */
    public void RegisterExit(String IdLatestStatusResponse, Double LatitudeOutput, Double LongitudeOutput) {
        // فراخوانی سرویس ثبت خروج کاربر
        final Call<Void> createdRequest = new InputOutputUserOperations().GetService().RegisterExit(IdLatestStatusResponse, LatitudeOutput, LongitudeOutput);
        createdRequest.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    view.removeWait();
                    view.showMessage("با موفقیت خروج ثبت شد.");
                    view.registerExit();
                } else {
                    view.removeWait();
                    view.showMessage("عدم ارتباط با سرور! دوباره سعی نمایید.");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                view.removeWait();
            }
        });
    }

}
