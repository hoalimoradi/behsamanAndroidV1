package com.ivib.OutputUser;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.ebanx.swipebtn.OnActiveListener;
import com.ebanx.swipebtn.SwipeButton;
import com.ivib.R;
import com.ivib.model.LatestStatusResponse;
import com.ivib.questionGroup.QuestionGroupActivity;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

public class OutputUserActivity extends AppCompatActivity implements OutputUserView {
    private static final int REQUEST_LOCATION = 1;

    private static Animation shakeAnimation;
    public SessionManager session;
    LinearLayout content_layout;
    AutofitTextView BranchesManagment;
    AutofitTextView DomainName;
    AutofitTextView BranchName;
    AutofitTextView Address;
    AutofitTextView PostalCode;
    AutofitTextView register_exit;
    LatestStatusResponse latestStatusResponse;
    OutputUserPresenter outputUserPresenter;
    private Activity mActivity;
    private LocationManager locationManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output_user);
        content_layout = findViewById(R.id.content_layout);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Load ShakeAnimation
        session = new SessionManager(getApplicationContext());
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        BranchesManagment = findViewById(R.id.BranchesManagment);
        DomainName = findViewById(R.id.DomainName);
        BranchName = findViewById(R.id.BranchName);
        Address = findViewById(R.id.Address);
        PostalCode = findViewById(R.id.PostalCode);
        register_exit = findViewById(R.id.register_exit);
        outputUserPresenter = new OutputUserPresenter(this, this);

        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
        latestStatusResponse = globalVariable.getLatestStatusResponse();

        BranchesManagment.setText(globalVariable.getLatestStatusResponse().getBranchesManagenemt());
        DomainName.setText(globalVariable.getLatestStatusResponse().getBranchDomainName());
        BranchName.setText(globalVariable.getLatestStatusResponse().getBranchName());
        Address.setText(globalVariable.getLatestStatusResponse().getBranchAddress());
        PostalCode.setText(globalVariable.getLatestStatusResponse().getBranchPostalCode());

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        register_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ثبت خروج کاربر
                getLocation();
                GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();

                outputUserPresenter.RegisterExit(latestStatusResponse.getId(), globalVariable.getLatitude(), globalVariable.getLongitude());

            }
        });

                
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }


    @Override
    public void registerExit() {
        finish();
    }

    @Override
    public void showMessage(String text) {
        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));
    }

    @Override
    public void hideRetry() {

    }


    /**
     * دریافت موقعیت مکانی حال حاضر کاربر
     */
    private void getLocation() {

        // بررسی مجوز استفاده از جی پی اس
        if (ActivityCompat.checkSelfPermission(OutputUserActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (OutputUserActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OutputUserActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            try {
                Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();

                if (location != null) {
                    globalVariable.setLatitude(location.getLatitude());
                    globalVariable.setLongitude(location.getLongitude());

                } else if (location1 != null) {
                    globalVariable.setLatitude(location1.getLatitude());
                    globalVariable.setLongitude(location1.getLongitude());

                } else if (location2 != null) {
                    globalVariable.setLatitude(location2.getLatitude());
                    globalVariable.setLongitude(location2.getLongitude());
                }
            } catch (Exception ignored) {

            }
        }
    }



}
