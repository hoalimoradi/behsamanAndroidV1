package com.ivib.OutputUser;

public interface OutputUserView {

    void showWait();

    void removeWait();

    void registerExit();

    void showMessage(String text);

    void hideRetry();
}
