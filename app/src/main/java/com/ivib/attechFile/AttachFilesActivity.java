package com.ivib.attechFile;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ivib.R;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.ivib.data.remote.QuestionsOperations;
import com.ivib.model.AttachFile;
import com.ivib.model.CheckList;
import com.ivib.userCheckLists.UserCheckListAdapter;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.IDN;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttachFilesActivity extends AppCompatActivity implements AttachFilesView {

    public static final String TAG = "  AttachFilesActivity ";

    public static final int RequestPermissionCode = 1;
    public static final int CAMERA_REQUEST = 10;
    public ImageView imageView, cameraImageButton;
    public File file;
    public String Id;
    public Uri uri;
    public Intent CamIntent;
    public Intent CropIntent;
    public int width, height;
    public SessionManager session;
    private Context context;
    private GlobalClass GlobalVariable;
    private AttachFilesView view;
    private AutofitTextView noImage;
    public RecyclerView list;
    private AttachFilesPresenter presenter;
    private AttachFilesAdapter adapter;
    private String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
private Activity mActivity;
    String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attach_files);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);
        view = this;
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        setResult(CAMERA_REQUEST);
        context = getApplicationContext();
        GlobalVariable = (GlobalClass) context.getApplicationContext();
        session = new SessionManager(getApplicationContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView mtitle = findViewById(R.id.mtitle);
        list = findViewById(R.id.list);
        imageView = findViewById(R.id.imageView);
        noImage= findViewById(R.id.noImage);
        cameraImageButton = findViewById(R.id.cameraImageButton);

        Intent intent = getIntent();

        if (null != intent) {

            Id = intent.getStringExtra("Id");
           Log.e(TAG, Id + " ای دی توی اتچ");
        }
      // EnableRuntimePermission();

        presenter = new AttachFilesPresenter(this,this);
        presenter.getAttachFileList(Id);

        if (Build.VERSION.SDK_INT < 23) {
            //Do not need to check the permission
        } else {
            if (checkPermissions()) {
              //  cameraImageButton.setVisibility(View.GONE);
                //If you have already permitted the permission
            }
        }

        // کلیک بر روی دکمه دوربین
        cameraImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickImageFromCamera();
            }
        });

    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    /**
     * کلیک بر روی دکمه دوربین
     */
    public void ClickImageFromCamera() {
        try {
            CamIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            //   file = new File(Environment.getExternalStorageDirectory(),"files" + String.valueOf(System.currentTimeMillis()) + ".jpg");
            file = new File(Environment.getExternalStorageDirectory(),
                    "files" + String.valueOf(System.currentTimeMillis()) + ".jpg");
            uri = Uri.fromFile(file);
            Log.e(TAG,"");
            CamIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, uri);
            CamIntent.putExtra("return-data", true);
            startActivityForResult(CamIntent, 0);
        }catch (Exception e){
            Log.e(TAG," Exception e " + e.getMessage());
            Log.e(TAG," Exception e " + e.getStackTrace());
            finish();
        }

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0 && resultCode == RESULT_OK) {
            // بریدن عکس
            Log.e(TAG,"AttachFilesActivity  onActivityResult requestCode == 0 && resultCode == RESULT_OK   ImageCropFunction(); ");

            ImageCropFunction();

        } else if (requestCode == 2) {
            if (data != null) {
                Log.e(TAG,"AttachFilesActivity  onActivityResult requestCode == 2 && data != null   ImageCropFunction(); ");

                uri = data.getData();
                // بریدن عکس
                ImageCropFunction();
            }
        } else if (requestCode == 1) {
            if (data != null) {
                Log.e(TAG,"AttachFilesActivity  onActivityResult requestCode == 1 && data != null   ImageCropFunction(); ");


                Bundle bundle = data.getExtras();
                Bitmap bitmap = null;
                if (bundle != null) {
                    bitmap = bundle.getParcelable("data");
                }

                // افزودن اطلاعات تصویر در پایگاه داده فایل های سوالات
            //    String url = "file://"+ uri.getPath().toString();
                Log.e(TAG, Id + "  id چ");
                AddToDbQuestionFile(Id, uri.getPath());

                imageView.setImageBitmap(bitmap);
                noImage.setVisibility(View.GONE);
                presenter.getAttachFileList(Id);

                //TODO check
                file = null;
                uri = null;

            }
        }
    }

    /**
     * تابع بریدن تصویر
     */
    public void ImageCropFunction() {

        try {
            CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.setDataAndType(uri, "image/*");
            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 380);
            CropIntent.putExtra("outputY", 380);
            CropIntent.putExtra("aspectX", 3);
            CropIntent.putExtra("aspectY", 2);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);
            startActivityForResult(CropIntent, 1);
            Log.e(TAG,"AttachFilesActivity    ImageCropFunction(); ");

        } catch (Exception ignored) {
            Log.e(TAG,"    ImageCropFunction(); Exception" + ignored.toString());
        }
    }

    /**
     * بررسی مجوز استفادهه از دوربین
     *
     *
     *
     */
    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(AttachFilesActivity.this, Manifest.permission.CAMERA)) {
           // Toast.makeText(AttachFilesActivity.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(AttachFilesActivity.this, new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);
        }



    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                   // Toast.makeText(AttachFilesActivity.this, "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
                } else {
                   // Toast.makeText(AttachFilesActivity.this, "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



//    public void EnableRuntimePermission() {
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(AttachFilesActivity.this, Manifest.permission.CAMERA)) {
//            // Toast.makeText(AttachFilesActivity.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();
//        } else {
//            ActivityCompat.requestPermissions(AttachFilesActivity.this, new String[]{
//                    Manifest.permission.CAMERA}, RequestPermissionCode);
//        }
//
//
//
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
//        switch (RC) {
//            case RequestPermissionCode:
//                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Toast.makeText(AttachFilesActivity.this, "Permission Granted, Now your application can access CAMERA.", Toast.LENGTH_LONG).show();
//                } else {
//                    // Toast.makeText(AttachFilesActivity.this, "Permission Canceled, Now your application cannot access CAMERA.", Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * افزودن اطلاعات فایل سوالات در جدول فایل ها
     *
     * @param questionId شناسه سوال
     * @param uriFile    آدرس فایل
     */
    private void AddToDbQuestionFile(String questionId, String uriFile) {
        dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
        QuestionsFilesInfo questionsFilesInfo = new QuestionsFilesInfo(0, GlobalVariable.getCheckListInfoId(), questionId, uriFile);
        dbQuestionsFileOp.Add(questionsFilesInfo);
    }


    @Override
    public void showWait() {

    }

    @Override
    public void removeWait() {

    }

    @Override
    public void showEmpty() {

    }

    @Override
    public void hideEmpty() {

    }

    @Override
    public void retry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showMessage(String text) {

    }

    @Override
    public void getAttachFileList(final List<AttachFile> attachFiles) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        list.setLayoutManager(llm);


        adapter = new AttachFilesAdapter(getApplicationContext(),attachFiles,
                new AttachFilesAdapter.OnItemClickListener() {


                    @Override
                    public void onClick(View v, int position) {

                        Bitmap bitmap1 = BitmapFactory.decodeFile(attachFiles.get(position).getUrlString());
                        imageView.setImageBitmap( bitmap1);

//                        if (attachFiles.get(position).isLocal()){
//                            Bitmap bitmap1 = BitmapFactory.decodeFile(attachFiles.get(position).getUrlString());
//                            imageView.setImageBitmap( bitmap1);
//                        }else {
//
//                            Picasso.with(getApplication()).load(attachFiles.get(position).getUrlString()).into(imageView);
//                        }

                    }

                    @Override
                    public void onDelete(View v, int position) {

                        Log.e(TAG,"onDelete presenter.deleteFromDbQuestionFile( : " +  position);

                        presenter.deleteFromDbQuestionFile(attachFiles.get(position));
                        removeItem(position);
                        checkImagreViewIsNull();

                    }
                });
        adapter.notifyDataSetChanged();
        list.setAdapter(adapter);


    }

    public void removeItem(int position){
        adapter.removeItem(position);
        if (adapter.getItemCount()==0)
        {
            imageView.setImageDrawable(null);
        }

    }
    @Override
    public void clearList() {

    }

    public void checkImagreViewIsNull(){
        if ( imageView.getDrawable() == null){
            noImage.setVisibility(View.VISIBLE);

        }else {
            noImage.setVisibility(View.GONE);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
        imageView.setImageDrawable(null);
    }
}
