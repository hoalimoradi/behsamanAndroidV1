package com.ivib.attechFile;

import com.ivib.model.AttachFile;
import com.ivib.model.UserNotification;

import java.util.List;

/**
 * Created by ho on 6/26/2018 AD.
 */

public interface AttachFilesView {
    void showWait();
    void removeWait();
    void showEmpty();
    void hideEmpty();
    void retry();
    void hideRetry();
    void showMessage(String text);
    void getAttachFileList(List<AttachFile> attachFiles);
    void clearList();
}
