package com.ivib.attechFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.model.AttachFile;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.questionGroup.QuestionGroupAdapter;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.squareup.picasso.Picasso;

import org.xml.sax.helpers.LocatorImpl;

import java.util.List;

/**
 * Created by ho on 6/26/2018 AD.
 */

public class AttachFilesAdapter extends RecyclerView.Adapter<AttachFilesAdapter.ViewHolder> {

    private final AttachFilesAdapter.OnItemClickListener listener;
    private List<AttachFile> data;
    private Context context;

    AttachFilesAdapter(Context context, List<AttachFile> data, AttachFilesAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public AttachFilesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_attach_files_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new AttachFilesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AttachFilesAdapter.ViewHolder holder, int position) {


        if (data.get(position).isLocal()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;

            Bitmap bitmap1 =BitmapFactory.decodeFile(data.get(position).getUrlString(),options);
            holder.image.setImageBitmap( bitmap1);
            Log.e("onBindViewHolder","data.get(position).getUrlString() : " + data.get(position).getUrlString());
        }else {

            Log.e("onBindViewHolder"," Picasso data.get(position).getUrlString() : " + data.get(position).getUrlString());

            Picasso.with(context).load(data.get(position).getUrlString()).into(holder.image);
        }


    }

    @Override
    public int getItemCount() {
        return  data == null ? 0 : data.size();

    }


    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public interface OnItemClickListener {
        void onClick(View v, int position);
        void onDelete(View v, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image, delete;

        ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(v, getAdapterPosition());
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDelete(v, getAdapterPosition());
                }
            });
        }


    }
}

