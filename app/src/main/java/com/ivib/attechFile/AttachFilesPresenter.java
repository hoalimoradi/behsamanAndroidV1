package com.ivib.attechFile;

import android.content.Context;
import android.util.Log;

import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.ivib.data.remote.QuestionsOperations;
import com.ivib.model.AttachFile;
import com.ivib.model.UploadedImage;
import com.ivib.utility.GlobalClass;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttachFilesPresenter {
    public static final String TAG = "AttachFilesPresenter";
    private final Context context;
    private final AttachFilesView view;

    AttachFilesPresenter(Context context, AttachFilesView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت عکس های آپلود شده و موجود در پایگاه داده
     */
    public void getAttachFileList( final String questionId) {
        try {

            GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
            int checkListInfoId= globalVariable.getCheckListInfoId();
            // لیست فایل های ثبت شده
            final List<AttachFile> results = new ArrayList<>();

            // یافتن اطلاعات چک لیست ثبت شده در پایگاه داده
            dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
            final CheckListInfo checkListInfo = dbCheckListOp.GetById(checkListInfoId);

            // یافتن اطلاعات فایل های چک لیست و سوال
            dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
            final List<QuestionsFilesInfo> QuestionsFilesList = dbQuestionsFileOp.GetAll(checkListInfoId, questionId);

            // اگر فایلی در پایگاه داده وجود داشت
            if (QuestionsFilesList != null) {

                // جستجو به ازای هر فایل
                for (QuestionsFilesInfo item : QuestionsFilesList) {

                    // افزودن فایل به لیست فایل ها
                    AttachFile attachFile = new AttachFile(item.Id, checkListInfo.CheckListId, questionId, item.FileAddress, true);
                    results.add(attachFile);
                }
            }

            if (checkListInfo !=null && checkListInfo.CheckListId != null && checkListInfo.CheckListId.length() >0){
                view.showWait();
                view.hideEmpty();
                view.hideRetry();

                // فراخوانی عکس های سوال
                final Call<List<UploadedImage>> createdRequest = new QuestionsOperations().GetService().GetQuestionAnswerFiles(checkListInfo.CheckListId, questionId);
                createdRequest.enqueue(new Callback<List<UploadedImage>>() {
                    @Override
                    public void onResponse(Call<List<UploadedImage>> call, Response<List<UploadedImage>> response) {
                        if (response.isSuccessful()) {
                            view.removeWait();

                            // پر کردن عکس های دریافت شده در لیست
                            if (response.body() != null) {
                                // جستجو به ازای هر فایل
                                for (UploadedImage item : response.body()) {

                                    // افزودن فایل به لیست فایل ها
                                    AttachFile attachFile = new AttachFile(Integer.parseInt(item.Id), checkListInfo.CheckListId, questionId, item.Uri, false);
                                    results.add(attachFile);
                                }
                            }
                        } else {
                            view.removeWait();
                        }

                        view.getAttachFileList(results);
                        Log.e(TAG,"if  results.size()   view.getAttachFileList(results);  " + results.size());
                    }

                    @Override
                    public void onFailure(Call<List<UploadedImage>> call, Throwable throwable) {
                        view.removeWait();
                        view.retry();
                    }
                });
            }else {
                Log.e(TAG," else  results.size()  " + results.size());
                view.getAttachFileList(results);
            }


        }catch (Exception e){
            Log.e(TAG," Exception e " + e.getMessage());
            Log.e(TAG," Exception e " + e.getStackTrace());
        }

        // بررسی وجود فایل ها برای چک لیست و سوال مربوطه از سرور



    }


    /**
     * حذف تصویر یک سوال
     *
     * @param attachFile مدل فایل یک سوال
     */
    public void deleteFromDbQuestionFile(AttachFile attachFile) {
        // اگر فایل در حافظه گوشی وجود دارد
        if (attachFile.isLocal()) {
            // یافتن اطلاعات فایل از پایگاه داده
            dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
            QuestionsFilesInfo model = dbQuestionsFileOp.Get(attachFile.getId());


            // حذف فایل از حافظه گوشی و پایگاه داده
            try {
                File file = new File(model.FileAddress);
                boolean deleted = file.delete();
                if (deleted) {
                    dbQuestionsFileOp.Delete(model.CheckListInfoId, model.QuestionId);

                }
            }catch (Exception e){

            }


        }
        // اگر فایل در سرور وجود دارد
        else {
            view.showWait();
            view.hideEmpty();
            view.hideRetry();

            // فراخوانی حذف تصویر یک سوال
            final Call<Void> createdRequest = new QuestionsOperations().GetService().DeleteQuestionAnswerFiles(attachFile.getId() + "");
            createdRequest.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        view.removeWait();

                    } else {
                        view.removeWait();
                        view.retry();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable throwable) {
                    view.removeWait();
                    view.retry();
                }
            });
        }


    }

}
