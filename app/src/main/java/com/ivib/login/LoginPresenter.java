package com.ivib.login;

import android.content.Context;

class LoginPresenter {
    private final LoginView view;
    private final Context context;

    LoginPresenter(LoginView view, Context context) {
        this.view = view;
        this.context = context;
    }

}