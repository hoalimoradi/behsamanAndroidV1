package com.ivib.login;

public interface LoginView {

    void showWait();

    void removeWait();

    void startMainActivity();

    void showMessage(String text);

}