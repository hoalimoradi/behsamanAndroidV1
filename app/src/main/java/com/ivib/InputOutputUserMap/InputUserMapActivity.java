package com.ivib.InputOutputUserMap;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ivib.R;
import com.ivib.utility.SessionManager;

public class InputUserMapActivity extends FragmentActivity implements OnMapReadyCallback {

    public SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_user_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        session = new SessionManager(getApplicationContext());
       // session.checkLogin();
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Add a marker in Sydney and move the camera 35.730966, 51.412829
        LatLng origin = new LatLng(35.73, 51.41);
        googleMap.addMarker(new MarkerOptions().position(origin).title("محل شما"));

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
    }
}
