package com.ivib.questionsByGroupid;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.attechFile.AttachFilesActivity;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class QuestionsByGroupidActivity extends AppCompatActivity implements QuestionsByGroupidView {
    public static final String TAG = "QuestionsByGroupid";
    private static final int REQUEST_CODE = 5;
    public static final int CAMERA_REQUEST = 10;
    public int index;
    public SessionManager session;
    private String GroupId = "";
    private String GroupName = "";
    private TextView mtitle;
    private RecyclerView list;
    private LinearLayout retryLinearLayout, empty_LinearLayout, nextLinearLayout, previousLinearLayout;
    private ProgressBar progress;
    private List<Group> globalQGL;
    private List<String> globalCQL;
    private List<Question> globalQL;
    private List<Question> questionListByGroupID = new ArrayList<>();
private Activity mActivity;
    private QuestionsByGroupIdAdapter adapter;
    public int rowPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_by_groupid);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setResult(REQUEST_CODE);
        session = new SessionManager(getApplicationContext());


        mtitle = findViewById(R.id.mtitle);
        list = findViewById(R.id.list);
        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        nextLinearLayout = findViewById(R.id.nextLinearLayout);
        previousLinearLayout = findViewById(R.id.previousLinearLayout);
        progress = findViewById(R.id.progress);

        // دریافت اطلاعات گروه از صفحه گروه ها
        Intent intent = getIntent();
        if (null != intent) {
            GroupId = intent.getStringExtra("GroupId");
            GroupName = intent.getStringExtra("GroupName");
            mtitle.setText(GroupName);
        }

        // دریافت اطلاعات گروه ها و سوالات از مدل عمومی
        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
        globalQL = globalVariable.getGlobalQL();
        globalCQL = globalVariable.getGlobalCQL();
        globalQGL = globalVariable.getGlobalQGL();

        // نمایش دکمه های گروه قبلی و بعدی زیر لیست سوالات در صورت ابتدا و انتهای هر گروه
        index = getIndex(GroupName);
        if (index == 0) {
            showNextOnly();
        } else if (index == globalQGL.size() - 1) {
            showPreviousOnly();
        }

        // دریافت اطلاعات سوالات با توجه به گروه آن
        getQuestionsByGroupId(GroupId);

        // کلیک بر روی دکمه گروه بعدی
        nextLinearLayout.setOnClickListener(new View.OnClickListener() {

            /**
             * کلیک بر روی دکمه گروه بعدی
             * @param v ویو
             */
            @Override
            public void onClick(View v) {

                index = getIndex(GroupName);
                if (index < globalQGL.size() -1) {
                    index = index + 1;
                    GroupId = globalQGL.get(index).getId();
                    GroupName = globalQGL.get(index).getName();
                    mtitle.setText(GroupName);
                    clearList();
                    showNextAndPrevious();

                    if (index == 0) {
                        showNextOnly();
                    } else if (index == globalQGL.size() - 1) {
                        showPreviousOnly();
                    }
                    // دریافت اطلاعات سوالات با توجه به گروه آن
                    getQuestionsByGroupId(GroupId);
                } else {
                    showPreviousOnly();
                }
            }
        });

        // کلیک بر روی دکمه گروه قبلی
        previousLinearLayout.setOnClickListener(new View.OnClickListener() {

            /**
             * کلیک بر روی دکمه گروه قبلی
             * @param v ویو
             */
            @Override
            public void onClick(View v) {

                index = getIndex(GroupName);
                if (index > 0) {
                    index = index - 1;
                    GroupId = globalQGL.get(index).getId();
                    GroupName = globalQGL.get(index).getName();
                    mtitle.setText(GroupName);
                    clearList();
                    showNextAndPrevious();
                    if (index == 0) {
                        showNextOnly();
                    } else if (index == globalQGL.size() - 1) {
                        showPreviousOnly();
                    }
                    // دریافت اطلاعات سوالات با توجه به گروه آن
                    getQuestionsByGroupId(GroupId);

                } else {
                    showNextOnly();
                }
            }
        });



    }

    /**
     * دریافت ایندکس یک گروه بر اساس نام آن
     *
     * @param GroupName نام گروه
     * @return int ایندکس گروه
     */
    public int getIndex(String GroupName) {
        int index = -1;
      //  int index = 0;
        if (globalQGL != null){
            Log.e("سایز لیست واسه اینمدکس","getIndex   globalQGL.size() : " + globalQGL.size());
            for (int i = 0; i < globalQGL.size(); i++) {
                if (globalQGL.get(i).getName().equalsIgnoreCase(GroupName)) {
                    index = i;
                    return index;
                }
            }
        }

        return index;
    }

    public void showNextOnly() {
        nextLinearLayout.setVisibility(View.VISIBLE);
        previousLinearLayout.setVisibility(View.GONE);
    }

    public void showPreviousOnly() {
        nextLinearLayout.setVisibility(View.GONE);
        previousLinearLayout.setVisibility(View.VISIBLE);
    }

    public void showNextAndPrevious() {
        nextLinearLayout.setVisibility(View.VISIBLE);
        previousLinearLayout.setVisibility(View.VISIBLE);
    }

    /**
     * دریافت سوالات یک گروه
     *
     * @param groupId شناسه گروه
     */
    public void getQuestionsByGroupId(final String groupId) {
        // خالی گردن سوالات یک گروه
        questionListByGroupID.clear();

        // پر کردن لیست سوالات گروه
        for (Question question : globalQL) {
            if (!globalCQL.contains(question.getId()) && groupId.equals(question.getQuestionGroupId())) {
                questionListByGroupID.add(question);
            }
        }

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        // آداپتر سوالات یک گروه
        adapter = new QuestionsByGroupIdAdapter(getApplicationContext(), questionListByGroupID,
                new QuestionsByGroupIdAdapter.OnItemClickListener() {
                    /**
                     * کلیک بر روی دکمه دوربین
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickCamera(View v, int position) {
                        // رفتن به صفحه لیست تصاویر
                        Intent intent = new Intent(getApplicationContext(), AttachFilesActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("Id", questionListByGroupID.get(position).getId());
                        intent.putExtras(bundle);
                       // startActivity(intent);
                        startActivityForResult(intent,CAMERA_REQUEST );

                        Log.e(TAG," startActivityForResult(intent,CAMERA_REQUEST );" );

                        rowPosition = position;
                        Log.e("   پوزیشن سطر که " ,position + "  ");


                    }

                    /**
                     * کلیک بر روی دکمه راهنمای سوال
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickHelp(View v, final int position) {
                        // اگر هم توضیحات داشت و هم فایل دانلود
                        if (questionListByGroupID.get(position).getDescription() != null && !questionListByGroupID.get(position).getStandardQuestionFile().isEmpty()) {
                            Typeface font = Typeface.createFromAsset(getAssets(), "IRANSansMobile.ttf");
                            AlertDialog dialog = new AlertDialog.Builder(QuestionsByGroupidActivity.this)
                                    .setMessage(questionListByGroupID.get(position).getDescription())
                                    .setCancelable(true)
                                    .setPositiveButton("دانلود فایل", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(questionListByGroupID.get(position).getStandardQuestionFile()));
                                                startActivity(browserIntent);
                                            } catch (Exception ignored) {
                                            }
                                        }
                                    })
                                    .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).show();
                            TextView textView = dialog.findViewById(android.R.id.message);
                            if (textView != null) {
                                textView.setTypeface(font);
                            }
                            Button btn1 = dialog.findViewById(android.R.id.button1);
                            if (btn1 != null) {
                                btn1.setTypeface(font);
                            }
                            Button btn2 = dialog.findViewById(android.R.id.button2);
                            if (btn2 != null) {
                                btn2.setTypeface(font);
                            }
                        }
                        // اگر توضیحات نداشت اما فایل دانلود داشت
                        else if (questionListByGroupID.get(position).getDescription() == null && !questionListByGroupID.get(position).getStandardQuestionFile().isEmpty()) {
                            Typeface font = Typeface.createFromAsset(getAssets(), "IRANSansMobile.ttf");
                            AlertDialog dialog = new AlertDialog.Builder(QuestionsByGroupidActivity.this)
                                    .setMessage("لطفا بر روی دانلود فایل کلیک کنید")
                                    .setCancelable(true)
                                    .setPositiveButton("دانلود فایل", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try {
                                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(questionListByGroupID.get(position).getStandardQuestionFile()));
                                                startActivity(browserIntent);
                                            } catch (Exception ignored) {
                                            }
                                        }
                                    })
                                    .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).show();

                            TextView textView = dialog.findViewById(android.R.id.message);
                            if (textView != null) {
                                textView.setTypeface(font);
                            }
                            Button btn1 = dialog.findViewById(android.R.id.button1);
                            if (btn1 != null) {
                                btn1.setTypeface(font);
                            }
                            Button btn2 = dialog.findViewById(android.R.id.button2);
                            if (btn2 != null) {
                                btn2.setTypeface(font);
                            }
                        }
                        // اگر توضیحات داشت اما فایل دانلود نداشت
                        else if (questionListByGroupID.get(position).getDescription() != null && questionListByGroupID.get(position).getStandardQuestionFile().isEmpty()) {
                            Typeface font = Typeface.createFromAsset(getAssets(), "IRANSansMobile.ttf");
                            AlertDialog dialog = new AlertDialog.Builder(QuestionsByGroupidActivity.this)
                                    .setMessage(questionListByGroupID.get(position).getDescription())
                                    .setCancelable(true)
                                    .setPositiveButton("متوجه شدم", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    })
                                    .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).show();

                            TextView textView = dialog.findViewById(android.R.id.message);
                            if (textView != null) {
                                textView.setTypeface(font);
                            }
                            Button btn1 = dialog.findViewById(android.R.id.button1);
                            if (btn1 != null) {
                                btn1.setTypeface(font);
                            }
                            Button btn2 = dialog.findViewById(android.R.id.button2);
                            if (btn2 != null) {
                                btn2.setTypeface(font);
                            }
                        }
                    }

                });

        list.setAdapter(adapter);
    }

    public void updateisQuestionAnswered(int position){
        adapter.updateData(position);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void clearList() {

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // دریافت اطلاعات گروه ها و سوالات از مدل عمومی
//        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
//        globalQL = globalVariable.getGlobalQL();
//        globalCQL = globalVariable.getGlobalCQL();
//        globalQGL = globalVariable.getGlobalQGL();
        Log.e("سایز لیست واسه ","onActivityResult getIndex   globalQGL.size() : " + globalQGL.size());
        if (resultCode == Activity.RESULT_CANCELED) {
            // code to handle cancelled state
        }
        else if (requestCode == CAMERA_REQUEST) {
            updateisQuestionAnswered( rowPosition);
            Log.e("   onActivityResult " ,requestCode + " updateisQuestionAnswered  ");
            // code to handle data from CAMERA_REQUEST
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

//        // دریافت اطلاعات گروه ها و سوالات از مدل عمومی
//        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
//        globalQL = globalVariable.getGlobalQL();
//        globalCQL = globalVariable.getGlobalCQL();
//        globalQGL = globalVariable.getGlobalQGL();
        Log.e("سایز لیست واسه onResume","getIndex   globalQGL.size() : " + globalQGL.size());


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}

