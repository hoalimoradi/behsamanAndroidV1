package com.ivib.questionsByGroupid;

public interface QuestionsByGroupidView {

    void showWait();

    void removeWait();

    void showEmpty();

    void hideEmpty();

    void retry();

    void hideRetry();

    void clearList();

}
