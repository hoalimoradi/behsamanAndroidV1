package com.ivib.questionsByGroupid;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ivib.R;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.Entity.QuestionsInfo;
import com.ivib.model.CheckList;
import com.ivib.model.Question.Question;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;

import java.util.List;

public class QuestionsByGroupIdAdapter extends RecyclerView.Adapter<QuestionsByGroupIdAdapter.ViewHolder> {
    private final QuestionsByGroupIdAdapter.OnItemClickListener listener;
    private List<Question> data;
    private Context context;
    private boolean checkedSelectStatus = true;

    QuestionsByGroupIdAdapter(Context context, List<Question> data, QuestionsByGroupIdAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public QuestionsByGroupIdAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_questions_by_group_id_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new QuestionsByGroupIdAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final QuestionsByGroupIdAdapter.ViewHolder holder, final int position) {

        holder.title.setVisibility(View.VISIBLE);
        String title = " " + data.get(position).getOrder()  + " - " + data.get(position).getTitle();
        holder.title.setText(title);

        // نمایش آیکن سبز در صورتی که سوال پاسخ داده شده است
        if (isQuestionAnswered(data.get(position).getId())) {

            holder.isQuestionAnsweredImageView.setVisibility(View.VISIBLE);
            holder.isNotQuestionAnsweredImageView.setVisibility(View.GONE);
        } else {
            holder.isQuestionAnsweredImageView.setVisibility(View.GONE);
            holder.isNotQuestionAnsweredImageView.setVisibility(View.VISIBLE);
        }

        // سوال تشریحی
        if (data.get(position).getAnswerValue()) {
            holder.answerEditText.setVisibility(View.VISIBLE);
            holder.answerEditText.setText(data.get(position).getDefaultAnswerValue());
            holder.radioGroupAnswerLinearLayout.setVisibility(View.GONE);

            // آپدیت جواب تشریحی
            holder.answerEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(final Editable editable) {
                    // آپدیت جواب سوال تشریحی
                    UpdateGlobalAndDbQuestions(data.get(position).getId(), editable.toString(), "");
                    if (isQuestionAnswered(data.get(position).getId())) {
                        holder.isQuestionAnsweredImageView.setVisibility(View.VISIBLE);
                        holder.isNotQuestionAnsweredImageView.setVisibility(View.GONE);
                    } else {
                        holder.isQuestionAnsweredImageView.setVisibility(View.GONE);
                        holder.isNotQuestionAnsweredImageView.setVisibility(View.VISIBLE);

                    }
                }
            });
        }
        // سوال گزینه ای
        else {
            holder.answerEditText.setVisibility(View.GONE);
            holder.radioGroupAnswerLinearLayout.setVisibility(View.VISIBLE);
            holder.radioGroupAnswer.removeAllViews();

            // افزودن گزینه ها
            if (!data.get(position).getAnswerList().isEmpty()) {
                RadioButton[] radioButtonsAnswer = new RadioButton[data.get(position).getAnswerList().size()];
                LinearLayout.LayoutParams radioButtonsAnswer_question_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                for (int i = 0; i < data.get(position).getAnswerList().size(); i++) {
                    radioButtonsAnswer[i] = new RadioButton(context);
                    String rTitle = "  " + data.get(position).getAnswerList().get(i).getTitle() + "  ";
                    radioButtonsAnswer[i].setText(rTitle);
                    radioButtonsAnswer[i].setLayoutParams(radioButtonsAnswer_question_params);
                    Typeface font = Typeface.createFromAsset(context.getAssets(), "IRANSansMobile.ttf");
                    radioButtonsAnswer[i].setTypeface(font);
                    radioButtonsAnswer[i].setTextColor(Color.BLACK);
                    radioButtonsAnswer[i].setChecked(false);

                    try {
                        if (data.get(position).getAnswerList().get(i).getId().equals(data.get(position).getDefaultAnswerChoiseId())) {
                            radioButtonsAnswer[i].setChecked(true);
                        } else {
                            radioButtonsAnswer[i].setChecked(false);
                        }
                    } catch (Exception e) {
                        radioButtonsAnswer[i].setChecked(false);
                    }

                    radioButtonsAnswer[i].setHighlightColor(Color.GREEN);
                    holder.radioGroupAnswer.addView(radioButtonsAnswer[i]);
                }

                holder.radioGroupAnswer.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                    }
                });

                // آپدیت جواب گزینه ها
                holder.radioGroupAnswer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup rg, int checkedId) {
                        if (checkedId != -1 && checkedSelectStatus){
                            checkedSelectStatus = false;
                            for (int i = 0; i < rg.getChildCount(); i++) {
                            RadioButton btn = (RadioButton) rg.getChildAt(i);
                            if (btn.getId() == checkedId) {
                                btn.setChecked(true);

                                // آپدیت جواب سوال گزینه ای
                                UpdateGlobalAndDbQuestions(data.get(position).getId(), "", data.get(position).getAnswerList().get(i).getId());


                                if (isQuestionAnswered(data.get(position).getId())) {
                                    holder.isQuestionAnsweredImageView.setVisibility(View.VISIBLE);
                                    holder.isNotQuestionAnsweredImageView.setVisibility(View.GONE);
                                } else {
                                    holder.isQuestionAnsweredImageView.setVisibility(View.GONE);
                                    holder.isNotQuestionAnsweredImageView.setVisibility(View.VISIBLE);
                                }

                                // آپدیت لیست سوالات مخفی شده
                                GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
                                globalVariable.updateGlobalCQL();
                                List<String> cql = globalVariable.getGlobalCQL();
                                String groupId = data.get(position).getQuestionGroupId();

                                // خالی گردن سوالات دیتا
                                data.clear();

                                // پر کردن لیست سوالات دیتا
                                for (Question item : globalVariable.getGlobalQL()) {
                                    if (!cql.contains(item.getId()) && groupId.equals(item.getQuestionGroupId())) {
                                        data.add(item);
                                    }
                                }
                            } else {
                                btn.setChecked(false);
                            }
                        }
                            checkedSelectStatus = true;
                            notifyDataSetChanged();
                        }

                    }
                });
            }
        }

        // افزودن آیکن استاندارد سوال
        if (data.get(position).getDescription() != null || data.get(position).getStandardQuestionFile() != null) {
            holder.helpImageView.setVisibility(View.VISIBLE);
        } else {
            holder.helpImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return  data == null ? 0 : data.size();

    }
//
    public void updateData(int position) {
        notifyItemChanged(position);
    }
//    public void addItem(int position, CheckList checkList) {
//        data.add(position, checkList);
//        notifyItemInserted(position);
//    }

    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);

    }

    /**
     * آپدیت جواب سوالات داده شده هم در مدل عمومی و هم در پایگاه داده سوالات پاسخ داده شده
     *
     * @param questionId         شناسه سوال
     * @param defaultAnswerValue پاسخ تشریحی
     * @param defaultChoiceId    پاسخ گزینه ای
     */
    private void UpdateGlobalAndDbQuestions(String questionId, String defaultAnswerValue, String defaultChoiceId) {
        // آپدیت پاسخ سوالات در مدل عمومی
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        globalVariable.setQuestionDefaultAnswer(questionId, defaultAnswerValue, defaultChoiceId);

        // افزودن سوال در جدول سوالات پاسخ داده شده
        dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
        QuestionsInfo questionsInfo = new QuestionsInfo(0, globalVariable.getCheckListInfoId(), questionId);
        dbQuestionOp.Add(questionsInfo);
    }

    /**
     * بررسی سوال پاسخ داده شده
     *
     * @param questionId شناسه سوال
     * @return boolean وضعیت پاسخ
     */
    private boolean isQuestionAnswered(String questionId) {
        GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
        dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
        dbQuestionsFileOperations dbQuestionFileOp = new dbQuestionsFileOperations(context);
        boolean result = false;
        for (Question q : globalVariable.getGlobalQL()) {
            if (q.getId().equals(questionId)) {
                if (dbQuestionOp.IsExist(globalVariable.getCheckListInfoId(), q.getId())) {
                    if (q.getQuestionDocumentStatus() == 1) {
                        if (dbQuestionFileOp.IsExist(globalVariable.getCheckListInfoId(), q.getId())) {
                            result = true;
                        }
                    } else {
                        result = true;
                    }
                }
                break;
            }
        }
        return result;
    }

    public interface OnItemClickListener {
        void onClickCamera(View v, int position);

        void onClickHelp(View v, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView title;
        EditText answerEditText;
        LinearLayout radioGroupAnswerLinearLayout;
        RadioGroup radioGroupAnswer;
        ImageView attachImageView, helpImageView;
        ImageView isQuestionAnsweredImageView, isNotQuestionAnsweredImageView;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            answerEditText = itemView.findViewById(R.id.answerEditText);
            radioGroupAnswerLinearLayout = itemView.findViewById(R.id.radioGroupAnswerLinearLayout);
            radioGroupAnswer = itemView.findViewById(R.id.radioGroupAnswer);
            attachImageView = itemView.findViewById(R.id.attachImageView);
            helpImageView = itemView.findViewById(R.id.helpImageView);
            isQuestionAnsweredImageView = itemView.findViewById(R.id.isQuestionAnsweredImageView);
            isNotQuestionAnsweredImageView = itemView.findViewById(R.id.isNotQuestionAnsweredImageView);

            attachImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickCamera(v, getAdapterPosition());
                }
            });

            helpImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickHelp(v, getAdapterPosition());
                }
            });

        }
    }
}

