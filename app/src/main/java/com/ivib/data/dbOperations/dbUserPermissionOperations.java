package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.UserPermissionsInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

public class dbUserPermissionOperations {

    private Context mContext;

    public dbUserPermissionOperations(Context context) {
        mContext = context;
    }

    public List<UserPermissionsInfo> GetAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<UserPermissionsInfo, Integer> modelDao = dbHelper.getUserPermissionsInfoDao();
            return modelDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int AddAll(List<UserPermissionsInfo> input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<UserPermissionsInfo, Integer> modelAppDao = dbHelper.getUserPermissionsInfoDao();
            DeleteBuilder<UserPermissionsInfo, Integer> deleteBuilder = modelAppDao.deleteBuilder();
            deleteBuilder.delete();
            return modelAppDao.create(input);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
