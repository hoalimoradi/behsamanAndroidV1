package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.UserInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

public class dbUserOperations {
    private Context mContext;

    public dbUserOperations(Context context) {
        mContext = context;
    }

    public UserInfo Get(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<UserInfo, Integer> modelAppDao = dbHelper.getUserInfoDao();
            QueryBuilder<UserInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.limit(1);
            builderModelApp.orderBy("Id", false);
            return modelAppDao.queryForFirst(builderModelApp.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int Add(UserInfo input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<UserInfo, Integer> modelAppDao = dbHelper.getUserInfoDao();
            DeleteBuilder<UserInfo, Integer> deleteBuilder = modelAppDao.deleteBuilder();
            deleteBuilder.delete();
            return modelAppDao.create(input);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
