package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

public class dbCheckListOperations {
    private Context mContext;



    public dbCheckListOperations(Context context) {
        mContext = context;
    }

    public CheckListInfo GetById(int id){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            return modelAppDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CheckListInfo GetByBranchId(int checkListInfoId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            return modelAppDao.queryForId(checkListInfoId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CheckListInfo GetByCheckListId(String checkListId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListId",checkListId);
            return builderModelApp.queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean IsNotExistByCheckListId(String checkListId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListId",checkListId);
            CheckListInfo result = builderModelApp.queryForFirst();
            return result == null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean IsNotExistByBranchId(String branchId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("BranchId",branchId);
            CheckListInfo result = builderModelApp.queryForFirst();
            return result == null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<CheckListInfo> GetAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            return modelAppDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<CheckListInfo> GetAllIfNull(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListId","");
            return builderModelApp.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int Add(CheckListInfo input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            modelAppDao.create(input);

            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("BranchId",input.BranchId);
            CheckListInfo result = builderModelApp.queryForFirst();
            return result.getId();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int UpdateByCheckListId(String branchId,String checkListId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("BranchId",branchId);
            CheckListInfo result = builderModelApp.queryForFirst();

            if (result != null){
                result.CheckListId = checkListId;
                return modelAppDao.update(result);
            }

            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int UpdateByGlobalData(String branchId,byte[] globalData){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("BranchId",branchId);
            CheckListInfo result = builderModelApp.queryForFirst();

            if (result != null){
                result.GlobalData = globalData;
                return modelAppDao.update(result);
            }

            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int Delete(int id){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            DeleteBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.where().eq("Id",id);
            return builderModelApp.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void DeleteAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            DeleteBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int DeleteByBranchId(String branchId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<CheckListInfo, Integer> modelAppDao = dbHelper.getCheckListInfoDao();
            QueryBuilder<CheckListInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("BranchId",branchId);
            CheckListInfo result = builderModelApp.queryForFirst();

            if (result != null){
                return modelAppDao.delete(result);
            }
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
