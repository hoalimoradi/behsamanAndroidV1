package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.TokenInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

public class dbUserTokenOperations {

    private Context mContext;

    public dbUserTokenOperations(Context context) {
        mContext = context;
    }

    public TokenInfo Get(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<TokenInfo, Integer> modelAppDao = dbHelper.getTokenInfoDao();
            QueryBuilder<TokenInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.limit(1);
            builderModelApp.orderBy("TokenAppId", false);
            return modelAppDao.queryForFirst(builderModelApp.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int Add(TokenInfo input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<TokenInfo, Integer> modelAppDao = dbHelper.getTokenInfoDao();
            DeleteBuilder<TokenInfo, Integer> deleteBuilder = modelAppDao.deleteBuilder();
            deleteBuilder.delete();
            return modelAppDao.create(input);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
