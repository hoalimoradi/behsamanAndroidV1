package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.QuestionsInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class dbQuestionOperations {
    private Context mContext;

    public dbQuestionOperations(Context context) {
        mContext = context;
    }

    public boolean IsExist(int checkListInfoId, String questionId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            QueryBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId).and().eq("QuestionId",questionId);
            QuestionsInfo result = builderModelApp.queryForFirst();
            return result != null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<QuestionsInfo> GetAll(int checkListInfoId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            QueryBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId);
            return builderModelApp.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int Add(QuestionsInfo input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            QueryBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",input.CheckListInfoId).and().eq("QuestionId",input.QuestionId);
            QuestionsInfo result = builderModelApp.queryForFirst();

            if (result == null){
                return modelAppDao.create(input);
            }

            return result.Id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int Delete(int checkListInfoId,String questionId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            QueryBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId).and().eq("QuestionId",questionId);
            QuestionsInfo result = builderModelApp.queryForFirst();

            if (result != null){
                return modelAppDao.delete(result);
            }

            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int DeleteAll(int checkListInfoId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            DeleteBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId);
            return builderModelApp.delete();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void DeleteAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsInfo, Integer> modelAppDao = dbHelper.getQuestionsInfoDao();
            DeleteBuilder<QuestionsInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.delete();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
