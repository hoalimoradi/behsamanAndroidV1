package com.ivib.data.dbOperations;

import android.content.Context;

import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class dbQuestionsFileOperations {
    private Context mContext;

    public dbQuestionsFileOperations(Context context) {
        mContext = context;
    }

    public QuestionsFilesInfo Get(int Id){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            return modelAppDao.queryForId(Id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<QuestionsFilesInfo> GetAll(int checkListInfoId,String questionId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            QueryBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId).and().eq("QuestionId",questionId);
            return builderModelApp.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<QuestionsFilesInfo> GetAll(int checkListInfoId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            QueryBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId);
            return builderModelApp.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<QuestionsFilesInfo> GetAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            QueryBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            return builderModelApp.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean IsExist(int checkListInfoId, String questionId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            QueryBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId).and().eq("QuestionId",questionId);
            QuestionsFilesInfo result = builderModelApp.queryForFirst();
            return result != null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int Add(QuestionsFilesInfo input){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();

            return modelAppDao.create(input);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int Delete(int checkListInfoId,String questionId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            QueryBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.queryBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId).and().eq("QuestionId",questionId);
            QuestionsFilesInfo result = builderModelApp.queryForFirst();

            if (result != null){
                return modelAppDao.delete(result);
            }

            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int DeleteAll(int checkListInfoId){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            DeleteBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.where().eq("CheckListInfoId",checkListInfoId);
            return builderModelApp.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void DeleteAll(){
        DBHelper dbHelper = new DBHelper(mContext);
        try {
            Dao<QuestionsFilesInfo, Integer> modelAppDao = dbHelper.getQuestionsFilesInfoDao();
            DeleteBuilder<QuestionsFilesInfo, Integer> builderModelApp = modelAppDao.deleteBuilder();
            builderModelApp.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
