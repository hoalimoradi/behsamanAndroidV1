package com.ivib.data.remote;

import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.model.UploadedImage;
import com.ivib.utility.ProgressRequestBody;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface QuestionsService {
    @FormUrlEncoded
    @POST("/api/UserCheckList/GetAllQuestion")
    Call<List<Question>> GetAllQuestion(@Field("CheckListId") String checkListId );

    @GET("/api/UserCheckList/GetAllGroup")
    Call<List<Group>> GetAllGroup();

    @FormUrlEncoded
    @POST("/api/UserCheckList/GetQuestionsByGroupId")
    Call<List<Question>> GetQuestionsByGroupId(@Field("Id") String id, @Field("CheckListId") String checkListId );

    @FormUrlEncoded
    @POST("/api/UserCheckList/UpdateQuestionAnswer")
    Call<Void> UpdateQuestionAnswer(@Field("CheckListId") String checkListId, @Field("QuestionId") String questionId, @Field("AnswerValue") String answerValue, @Field("AnswerChoiceId") String answerChoiceId );

    @Multipart
    @POST("/api/UserCheckList/AddQuestionAnswerFiles")
    Call<Void> AddQuestionAnswerFiles(@Part("upload\"; filename=\"sendedfile\" ") ProgressRequestBody file);

    @FormUrlEncoded
    @POST("/api/UserCheckList/DeleteQuestionAnswerFiles")
    Call<Void> DeleteQuestionAnswerFiles(@Field("MultimediaId") String multimediaId);

    @FormUrlEncoded
    @POST("/api/UserCheckList/GetQuestionAnswerFiles")
    Call<List<UploadedImage>> GetQuestionAnswerFiles(@Field("CheckListId") String checkListId, @Field("QuestionId") String questionId );
}
