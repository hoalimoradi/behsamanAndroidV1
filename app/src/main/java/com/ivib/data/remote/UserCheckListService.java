package com.ivib.data.remote;

import com.ivib.model.CheckList;
import com.ivib.model.LatestStatusResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface UserCheckListService {

    @GET("/api/UserCheckList/GetCheckLists")
    Call<List<CheckList>> GetCheckLists();

    @FormUrlEncoded
    @POST("/api/UserCheckList/IsExistCheckList")
    Call<Void> IsExistCheckList(@Field("BranchId") String BranchId);

    @FormUrlEncoded
    @POST("/api/UserCheckList/AddCheckList")
    Call<String> AddCheckList(@Field("BranchId") String BranchId , @Field("BranchLatitude") Double BranchLatitude, @Field("BranchLongitude") Double BranchLongitude);

    @FormUrlEncoded
    @POST("/api/UserCheckList/CheckListCompleteAll")
    Call<Void> CheckListCompleteAll(@Field("CheckListId") String checkListId);
}
