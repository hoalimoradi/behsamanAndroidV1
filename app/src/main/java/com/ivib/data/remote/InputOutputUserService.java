package com.ivib.data.remote;


import com.ivib.model.LatestStatusResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by ho on 6/7/2018 AD.
 */



public interface InputOutputUserService {
    @FormUrlEncoded
    @POST("/api/InputOutputUser/RegisterEntry")
    Call<Void> RegisterEntry(@Field("BranchId") String BranchId , @Field("LatitudeInput") Double LatitudeInput,@Field("LongitudeInput") Double LongitudeInput);



    @FormUrlEncoded
    @POST("/api/InputOutputUser/RegisterExit")
    Call<Void> RegisterExit(@Field("Id") String IdLatestStatusResponse , @Field("LatitudeOutput") Double LatitudeOutput,@Field("LongitudeOutput") Double LongitudeOutput);

    @GET("/api/InputOutputUser/LatestStatus")
    Call<LatestStatusResponse> LatestStatus();
}
