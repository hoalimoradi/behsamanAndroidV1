package com.ivib.data.remote;

import com.ivib.model.Token;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginServices {

    @FormUrlEncoded
    @POST("/Token")
    Call<Token> GetAccessToken(@Field("grant_type") String grandType, @Field("username") String username, @Field("password") String password);


}
