package com.ivib.data.remote;

/**
 * Created by ho on 5/31/2018 AD.
 */


import com.ivib.model.UserProfile.UserProfile;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserProfileServices {

    @GET("/api/UserProfile/Get")
    Call<UserProfile> GetUserProfile();





}
