package com.ivib.data.remote;

import com.ivib.model.UserNotification;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ho on 6/21/2018 AD.
 */

public interface NotificationService {

    @FormUrlEncoded
    @POST("/api/UserNotification/Get")
    Call<List<UserNotification>> UserNotification(@Field("NumberOfRecords") String NumberOfRecords , @Field("NumberOfSkip") String NumberOfSkip);


    @FormUrlEncoded
    @POST("/api/UserNotification/SendToAdmin")
    Call<Void> SendToAdmin(@Field("Content") String Content );


}
