package com.ivib.data.local.Entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "TokenInfo")
public class TokenInfo {

    @DatabaseField(generatedId = true)
    public int TokenAppId;

    @DatabaseField
    public String AccessToken;

    @DatabaseField
    public String TokenType;

    @DatabaseField
    public int ExpiresIn;

    @DatabaseField
    public String UserName;

    @DatabaseField
    public String Password;

    @DatabaseField
    public String Issued;

    @DatabaseField
    public String Expires;

    public TokenInfo() {
    }

    public TokenInfo(int tokenAppId, String accessToken, String tokenType, int expiresIn, String userName, String password, String issued, String expires) {
        TokenAppId = tokenAppId;
        AccessToken = accessToken;
        TokenType = tokenType;
        ExpiresIn = expiresIn;
        UserName = userName;
        Password = password;
        Issued = issued;
        Expires = expires;
    }

    public long getTokenAppId() {
        return TokenAppId;
    }

    public void setTokenAppId(int tokenAppId) {
        TokenAppId = tokenAppId;
    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getTokenType() {
        return TokenType;
    }

    public void setTokenType(String tokenType) {
        TokenType = tokenType;
    }

    public int getExpiresIn() {
        return ExpiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        ExpiresIn = expiresIn;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getIssued() {
        return Issued;
    }

    public void setIssued(String issued) {
        Issued = issued;
    }

    public String getExpires() {
        return Expires;
    }

    public void setExpires(String expires) {
        Expires = expires;
    }
}