package com.ivib.data.local;

/**
 * Created by ho on 5/31/2018 AD.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class HelperContext implements Parcelable {

    public static Activity mContext;

    public HelperContext(Activity context) {
        mContext=context;
    }


    protected HelperContext(Parcel in) {
    }

    public static final Creator<HelperContext> CREATOR = new Creator<HelperContext>() {
        @Override
        public HelperContext createFromParcel(Parcel in) {
            return new HelperContext(in);
        }

        @Override
        public HelperContext[] newArray(int size) {
            return new HelperContext[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}