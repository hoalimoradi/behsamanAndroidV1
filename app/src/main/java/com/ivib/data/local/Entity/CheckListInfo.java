package com.ivib.data.local.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "CheckListInfo")
public class CheckListInfo {
    @DatabaseField(generatedId = true)
    public int Id;

    @DatabaseField
    public String CheckListId;

    @DatabaseField
    public String BranchId;

    @DatabaseField
    public String Title;

    @DatabaseField
    public String CreateDateTime;

    @DatabaseField
    public String BranchArea;

    @DatabaseField
    public String BranchesManagement;

    @DatabaseField
    public String BranchDomainName;

    @DatabaseField
    public String BranchName;

    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    public byte[] GlobalData;

    public CheckListInfo(int id, String checkListId, String branchId, String title, String createDateTime, String branchArea, String branchesManagement, String branchDomainName, String branchName, byte[] globalData) {
        Id = id;
        CheckListId = checkListId;
        BranchId = branchId;
        Title = title;
        CreateDateTime = createDateTime;
        BranchArea = branchArea;
        BranchesManagement = branchesManagement;
        BranchDomainName = branchDomainName;
        BranchName = branchName;
        GlobalData = globalData;
    }

    public CheckListInfo() {
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCheckListId() {
        return CheckListId;
    }

    public void setCheckListId(String checkListId) {
        CheckListId = checkListId;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public byte[] getGlobalData() {
        return GlobalData;
    }

    public void setGlobalData(byte[] globalData) {
        GlobalData = globalData;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getCreateDateTime() {
        return CreateDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        CreateDateTime = createDateTime;
    }

    public String getBranchArea() {
        return BranchArea;
    }

    public void setBranchArea(String branchArea) {
        BranchArea = branchArea;
    }

    public String getBranchesManagement() {
        return BranchesManagement;
    }

    public void setBranchesManagement(String branchesManagement) {
        BranchesManagement = branchesManagement;
    }

    public String getBranchDomainName() {
        return BranchDomainName;
    }

    public void setBranchDomainName(String branchDomainName) {
        BranchDomainName = branchDomainName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }
}
