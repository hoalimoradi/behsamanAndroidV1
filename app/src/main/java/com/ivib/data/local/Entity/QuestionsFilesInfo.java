package com.ivib.data.local.Entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "QuestionsFilesInfo")
public class QuestionsFilesInfo {

    @DatabaseField(generatedId = true)
    public int Id;

    @DatabaseField
    public int CheckListInfoId;

    @DatabaseField
    public String QuestionId;

    @DatabaseField
    public String FileAddress;

    public QuestionsFilesInfo() {
    }

    public QuestionsFilesInfo(int id, int checkListInfoId, String questionId, String fileAddress) {

        Id = id;
        CheckListInfoId = checkListInfoId;
        QuestionId = questionId;
        FileAddress = fileAddress;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCheckListInfoId() {
        return CheckListInfoId;
    }

    public void setCheckListInfoId(int checkListInfoId) {
        CheckListInfoId = checkListInfoId;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getFileAddress() {
        return FileAddress;
    }

    public void setFileAddress(String fileAddress) {
        FileAddress = fileAddress;
    }



}
