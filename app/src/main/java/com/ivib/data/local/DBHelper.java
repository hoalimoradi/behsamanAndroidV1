package com.ivib.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ivib.R;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.ivib.data.local.Entity.QuestionsInfo;
import com.ivib.data.local.Entity.TokenInfo;
import com.ivib.data.local.Entity.UserInfo;
import com.ivib.data.local.Entity.UserPermissionsInfo;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "ivibdatabase.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<TokenInfo, Integer> tokenInfoDao;
    private Dao<QuestionsInfo, Integer> questionsInfoDao;
    private Dao<QuestionsFilesInfo, Integer> questionsFilesInfoDao;
    private Dao<CheckListInfo, Integer> checkListInfoDao;
    private Dao<UserInfo, Integer> userInfoDao;
    private Dao<UserPermissionsInfo, Integer> userPermissionsInfoDao;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);

    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {

        // handles database creation, it may look like the below
        try {
            TableUtils.createTable(connectionSource, CheckListInfo.class);
            TableUtils.createTable(connectionSource, QuestionsInfo.class);
            TableUtils.createTable(connectionSource, QuestionsFilesInfo.class);
            TableUtils.createTable(connectionSource, TokenInfo.class);
            TableUtils.createTable(connectionSource, UserInfo.class);
            TableUtils.createTable(connectionSource, UserPermissionsInfo.class);
        } catch (SQLException e) {
            Log.e(DBHelper.class.getName(), "Could not create a database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer)
    {
        // handles database upgrade
    }

    public Dao<CheckListInfo, Integer> getCheckListInfoDao() throws SQLException {
        if (checkListInfoDao == null) {
            checkListInfoDao = getDao(CheckListInfo.class);
        }
        return checkListInfoDao;
    }

    public Dao<QuestionsInfo, Integer> getQuestionsInfoDao() throws SQLException {
        if (questionsInfoDao == null) {
            questionsInfoDao = getDao(QuestionsInfo.class);
        }
        return questionsInfoDao;
    }

    public Dao<QuestionsFilesInfo, Integer> getQuestionsFilesInfoDao() throws SQLException {
        if (questionsFilesInfoDao == null) {
            questionsFilesInfoDao = getDao(QuestionsFilesInfo.class);
        }
        return questionsFilesInfoDao;
    }

    public Dao<TokenInfo, Integer> getTokenInfoDao() throws SQLException {
        if (tokenInfoDao == null) {
            tokenInfoDao = getDao(TokenInfo.class);
        }
        return tokenInfoDao;
    }

    public Dao<UserInfo, Integer> getUserInfoDao() throws SQLException {
        if (userInfoDao == null) {
            userInfoDao = getDao(UserInfo.class);
        }
        return userInfoDao;
    }

    public Dao<UserPermissionsInfo, Integer> getUserPermissionsInfoDao() throws SQLException {
        if (userPermissionsInfoDao == null) {
            userPermissionsInfoDao = getDao(UserPermissionsInfo.class);
        }
        return userPermissionsInfoDao;
    }

}
