package com.ivib.data.local.Entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "UserInfo")
public class UserInfo {

    @DatabaseField
    public String Id;

    @DatabaseField
    public int NewNotificationCount;

    @DatabaseField
    public String UserName;

    @DatabaseField
    public String FirstName;

    @DatabaseField
    public String LastName;

    @DatabaseField
    public String PhoneNumber;


    @DatabaseField
    public String ImageLastAddress;

    public UserInfo() {
    }

    public UserInfo(String id, int newNotificationCount, String userName, String firstName, String lastName,
                    String phoneNumber,String imageLastAddress) {
        Id = id;
        NewNotificationCount = newNotificationCount;
        UserName = userName;
        FirstName = firstName;
        LastName = lastName;
        PhoneNumber = phoneNumber;
        ImageLastAddress = imageLastAddress;
    }

    public String getImageLastAddress() {
        return ImageLastAddress;
    }

    public void setImageLastAddress(String imageLastAddress) {
        ImageLastAddress = imageLastAddress;
    }

    public int getNewNotificationCount() {
        return NewNotificationCount;
    }

    public void setNewNotificationCount(int newNotificationCount) {
        NewNotificationCount = newNotificationCount;
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }



}
