package com.ivib.data.local.Entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "QuestionsInfo")
public class QuestionsInfo {

    @DatabaseField(generatedId = true)
    public int Id;

    @DatabaseField
    public int CheckListInfoId;

    @DatabaseField
    public String QuestionId;

    public int getId() {
        return Id;
    }

    public QuestionsInfo(int id, int checkListInfoId, String questionId) {
        Id = id;
        CheckListInfoId = checkListInfoId;
        QuestionId = questionId;
    }

    public QuestionsInfo() {

    }

    public void setId(int id) {
        Id = id;
    }

    public int getCheckListInfoId() {
        return CheckListInfoId;
    }

    public void setCheckListInfoId(int checkListInfoId) {
        CheckListInfoId = checkListInfoId;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

}
