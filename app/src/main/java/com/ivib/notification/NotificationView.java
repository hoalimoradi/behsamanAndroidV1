package com.ivib.notification;

import com.ivib.model.CheckList;
import com.ivib.model.UserNotification;

import java.util.List;

/**
 * Created by ho on 6/22/2018 AD.
 */

public interface NotificationView {
    void showWait();
    void removeWait();
    void showEmpty();
    void hideEmpty();
    void retry();
    void hideRetry();
    void showMessage(String text);
    void getNotifList(List<UserNotification> checkLists);
    void clearList();
}
