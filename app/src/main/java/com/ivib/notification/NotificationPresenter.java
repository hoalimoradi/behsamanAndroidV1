package com.ivib.notification;

import android.content.Context;
import android.util.Log;

import com.ivib.data.remote.NotificationOperations;
import com.ivib.data.remote.UserCheckListOperations;
import com.ivib.model.CheckList;
import com.ivib.model.UserNotification;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ho on 6/21/2018 AD.
 */

public class NotificationPresenter {
    private final String TAG = "NotificationPresenter";

    private final Context context;
    private final NotificationView view;

    public NotificationPresenter(Context context, NotificationView view) {
        this.context = context;
        this.view = view;
    }


    public void getNotificationLists()
    {
        view.showWait();
        view.hideEmpty();
        view.hideRetry();

        final Call<List<UserNotification>> createdRequest= new NotificationOperations().GetService().UserNotification("40","0");
        createdRequest.enqueue(new Callback<List<UserNotification>>() {

            @Override
            public void onResponse(Call<List<UserNotification>> call, Response<List<UserNotification>> response) {
                if (response.isSuccessful()) {
                    if (response.body().isEmpty()) {
                        view.showEmpty();
                    }else {

                        view.getNotifList(response.body());
                    }
                    view.removeWait();
                } else {
                    view.removeWait();
                    view.retry();
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                }
            }

            @Override
            public void onFailure(Call<List<UserNotification>> call, Throwable throwable) {
                view.removeWait();
                view.retry();
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    public void send(String Content)
    {
        view.showWait();
        view.hideEmpty();
        view.hideRetry();

        final Call<Void> createdRequest= new NotificationOperations().GetService().SendToAdmin(Content);
        createdRequest.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {

                    view.removeWait();
                    view.showMessage("پیام شما فرستاده شد");
                } else {
                    Log.e(TAG," onResponse is null " );
                    view.removeWait();
                   // view.retry();
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Log.e(TAG, " onFailure " +  throwable.getMessage().toString());
                view.removeWait();
              //  view.retry();
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

}
