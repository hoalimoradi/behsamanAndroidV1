package com.ivib.notification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.model.CheckList;
import com.ivib.model.UserNotification;
import com.ivib.questionGroup.QuestionGroupPresenter;
import com.ivib.questionGroup.QuestionGroupView;
import com.ivib.tempCheckList.TempCheckListAdapter;
import com.ivib.tempCheckList.TempCheckListPresenter;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonProgressDialog;

import java.util.ArrayList;
import java.util.List;


public class NotificationActivity extends AppCompatActivity  implements NotificationView{

    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_CODE = 5;


    public SessionManager session;
    private Context context;
    private RecyclerView list;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout retryLinearLayout, empty_LinearLayout;
    private GlobalClass GlobalVariable;
    private dbCheckListOperations DBCheckListOp;
    private dbQuestionsFileOperations DBQuestionsFileOp;
    private ProgressBar progress;
    private NotificationPresenter presenter;
    private NotificationView view;
    private LocationManager locationManager;
    private static Animation shakeAnimation;
    private LinearLayout content_layout;
    private Button buttonAddNewCheckList;
    public String parent = "";
    public SwipeRefreshLayout swipeRefreshLayout ;
    public TextView mTitle;
    private EditText editText;
    private ImageView send;
    public String sendText = "";
    private Activity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
mActivity = this;
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);
        content_layout = (LinearLayout) findViewById(R.id.content_layout);


        session = new SessionManager(getApplicationContext());
        mTitle = findViewById(R.id.mtitle);
        editText = findViewById(R.id.editText);
        send = findViewById(R.id.send);
        list = findViewById(R.id.list);
        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        progress = findViewById(R.id.progress);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.material_red_200, R.color.material_red_300);
        swipeRefreshLayout.setBackgroundColor(Color.TRANSPARENT);
        view = this;
        presenter = new NotificationPresenter(this,view);
        presenter.getNotificationLists();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clearList();
                        presenter.getNotificationLists();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        editText.clearFocus();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                sendText = editable.toString();

            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.send(sendText);

                editText.setText("");
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String text) {

        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));

    }



    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void getNotifList(List<UserNotification> checkLists) {
       // List<UserNotification> data = new ArrayList<>();
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        NotificationAdapter adapter = new NotificationAdapter(getApplicationContext(), checkLists,
                new NotificationAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(UserNotification Item) {

                    }
                });
        list.setAdapter(adapter);

    }

    @Override
    public void clearList() {

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);


        List<UserNotification> data = new ArrayList<>();
        NotificationAdapter adapter = new NotificationAdapter(getApplicationContext(), data,
                new NotificationAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(UserNotification Item) {

                    }
                });
        list.setAdapter(adapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}
