package com.ivib.notification;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.model.UserNotification;
import com.ivib.questionGroup.QuestionGroupAdapter;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.richpath.RichPathView;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ho on 6/21/2018 AD.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private final NotificationAdapter.OnItemClickListener listener;
    private List<UserNotification> data;
    private Context context;

    public NotificationAdapter(Context context, List<UserNotification> data, NotificationAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_notification_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup)view);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {

        holder.click(data.get(position), listener);
        holder.ReceiverFullName.setText(data.get(position).getSenderFullName());
        holder.SenderFullName.setText(data.get(position).getSenderUserName());


        holder.title.setText(data.get(position).getContent());
        holder.date.setText(data.get(position).getCreateDate());

        Picasso.with(context)
                .load(data.get(position).getSenderPic())
                .placeholder(R.drawable.ic_person_24dp)
                .into( holder.profile_image);


    }

    @Override
    public int getItemCount() {

        return  data == null ? 0 : data.size();

    }

    public interface OnItemClickListener {
        void onClick(UserNotification Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView title , date;
        CircleImageView profile_image ;
        TextView ReceiverFullName, SenderFullName;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (AutofitTextView) itemView.findViewById(R.id.title);
            date = (AutofitTextView) itemView.findViewById(R.id.date);
            ReceiverFullName = (TextView) itemView.findViewById(R.id.ReceiverFullName);
            SenderFullName = (TextView) itemView.findViewById(R.id.SenderFullName);

            profile_image = (CircleImageView) itemView.findViewById(R.id.profile_image);





        }


        public void click(final UserNotification group, final NotificationAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(group);
                }
            });
        }
    }






}

