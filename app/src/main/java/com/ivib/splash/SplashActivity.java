package com.ivib.splash;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.ivib.R;
import com.ivib.data.local.DBHelper;
import com.ivib.data.local.Entity.TokenInfo;
import com.ivib.data.local.Entity.UserInfo;
import com.ivib.data.local.Entity.UserPermissionsInfo;
import com.ivib.data.remote.LoginOperations;
import com.ivib.data.remote.UserProfileOperations;
import com.ivib.login.LoginActivity;
import com.ivib.main.MainActivity;
import com.ivib.model.GetServerErrors;
import com.ivib.model.Token;
import com.ivib.model.UserProfile.Permission;
import com.ivib.model.UserProfile.UserProfile;
import com.ivib.utility.CheckConnection;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements SplashView {

    private static Context mContext;
    private Activity mActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mActivity = this;
        mContext = this;
        showWait();

        TokenInfo t = SingletonAppBase.getTokenInfo(mActivity);
        if ( t!= null){
            CheckBaseInfo(SingletonAppBase.getTokenInfo(mActivity).UserName, SingletonAppBase.getTokenInfo(mActivity).Password);
        }else {
            startLoginActivity();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        SingletonProgressDialog.Stop(mActivity);
    }

    /**
     * بررسی اطلاعات اصلی سیستم
     * @param username نام کاربری
     * @param password پسورد
     */
    private void CheckBaseInfo(String username, String password) {
        if (!new CheckConnection().isOnline(mActivity)) {
            // رفتن به صفحه آفلاین در صورت نداشتن اینترنت
            //  GotoIsOffline(this);
        } else {
            // لاگین کاربر
            LoginUser(username, password);
        }
    }


    /**
     * متد لاگین کاربر
     * @param username نام کاربری
     * @param password رمز عبور
     */
    private void LoginUser(final String username, final String password) {
        // خالی کردن کلاس های عمومی
        SingletonAppBase.setAccessToken("");
        SingletonAppBase.setTokenInfo(null);
        SingletonAppBase.setUserInfo(null);
        SingletonAppBase.setUserPermissionsInfo(null);

        // فراخوانی سرویس لاگین
        final Call<Token> createdRequest = new LoginOperations().GetService()
                .GetAccessToken("password", username, password);
        createdRequest.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {

                removeWait();
                if (response.isSuccessful()) {
                    // گرفتن اطلاعات توکن و ذخیره در دیتا بیس
                    if (GetTokenInfo(response.body(), password)) {
                        // گرفتن اطلاعات کاربر
                        GetUserInfo();
                    }
                } else if (response.code() == 401) {
                    // ریست توکن در صورت عدم لاگین موفق
                    SingletonAppBase.ResetToken(mActivity);
                } else {
                    Toast.makeText(SplashActivity.this, new GetServerErrors().FromStream(response.errorBody().byteStream()), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable throwable) {
                ShowExceptionMessage(null);
                removeWait();
            }
        });
    }

    /**
     * ذخیره سازی اطلاعات توکن در پایگاه داده
     * @param jsonObject مدل اطلاعات توکن
     * @param password پسورد
     * @return boolean وضعیت موفق یا ناموفق بودن ذخیره اطلاعات در پایگاه داده
     */
    private boolean GetTokenInfo(Token jsonObject, String password) {
        // ساخت مدل پایگاه داده
        TokenInfo tokenInfo = new TokenInfo(0, jsonObject.access_token,
                jsonObject.token_type, Integer.parseInt(jsonObject.expires_in),
                jsonObject.userName, password, jsonObject.issued,
                jsonObject.expires);

        // ذخیره توکن در پایگاه داده
        try {
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<TokenInfo, Integer> tokenAppDao = dbHelper.getTokenInfoDao();
            DeleteBuilder<TokenInfo, Integer> deleteBuilder = tokenAppDao.deleteBuilder();
            deleteBuilder.delete();
            QueryBuilder<TokenInfo, Integer> builder = tokenAppDao.queryBuilder();
            builder.limit(1);
            builder.orderBy("TokenAppId", false);

            int result = tokenAppDao.create(tokenInfo);
            if (result == 1) {
                // ذخیره اطلاعات توکن در مدل عمومی
                TokenInfo savedTokenInfo = tokenAppDao.queryForFirst(builder.prepare());
                SingletonAppBase.setTokenInfo(savedTokenInfo);
                SingletonAppBase.setAccessToken(savedTokenInfo.AccessToken);
            }

        } catch (SQLException e) {
            Toast.makeText(SplashActivity.this, getResources()
                    .getText(R.string.error_database_change), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }



    /**
     * دریافت اطلاعات کاربر از سرویس
     */
    private void GetUserInfo() {
        SingletonProgressDialog.Start(mActivity);

        // فراخوانی سرویس اطلاعات کاربر
        final Call<UserProfile> createdRequest = new UserProfileOperations()
                .GetService().GetUserProfile();
        createdRequest.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                SingletonProgressDialog.Stop(mActivity);
                if (response.isSuccessful()) {
                    // پر کردن اطلاعات کاربر
                    FillSingletonUserInfo(response.body());

                    // پر کردن مجوز های کاربر
                    FillSingletonPermissionInfo(response.body().getPermissions());
                    startMainActivity();
                } else if (response.code() == 401) {
                    // ریست توکن در صورت لاگین نا موفق
                    SingletonAppBase.ResetToken(mActivity);
                    startLoginActivity();
                } else {
                    ShowExceptionMessage(response.code());
                    startLoginActivity();
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable throwable) {
                startLoginActivity();

                ShowExceptionMessage(null);
            }
        });
    }


    /**
     * پر کردن اطلاعات کاربر در پایگاه داده
     *
     * @param userProfile مدل اطلاعات کاربر
     */
    private static void FillSingletonUserInfo(UserProfile userProfile) {
        try {
            // افزودن اطلاعات در پایگاه داده
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<UserInfo, Integer> userInfoDao = dbHelper.getUserInfoDao();
            DeleteBuilder<UserInfo, Integer> deleteBuilder = userInfoDao.deleteBuilder();
            deleteBuilder.delete();
            UserInfo userInfoViewModel = new UserInfo();
            userInfoViewModel.setId(userProfile.getId());
            userInfoViewModel.setFirstName(userProfile.getFirstName());
            userInfoViewModel.setLastName(userProfile.getLastName());
            userInfoViewModel.setUserName(userProfile.getUserName());
            userInfoViewModel.setImageLastAddress(userProfile.getImageLastAddress());
            userInfoViewModel.setNewNotificationCount(userProfile.getNewNotificationCount());

            userInfoDao.create(userInfoViewModel);
            UserInfo getUserInfo = userInfoDao.queryBuilder().where().eq("Id", userProfile.getId()).queryForFirst();

            // افزودن اطلاعات در کلاس عمومی
            SingletonAppBase.setUserInfo(getUserInfo);
        } catch (Exception e) {
            Toast.makeText(mContext, R.string.message_reinstall_app, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * پر کردن اطلاعات مجوز های کاربر در پایگاه داده
     *
     * @param permissions لیست مجوز های کاربر
     */
    private static void FillSingletonPermissionInfo(List<Permission> permissions) {
        try {
            // افزودن اطلاعات در پایگاه داده
            DBHelper dbHelper = new DBHelper(mContext);
            Dao<UserPermissionsInfo, Integer> userPermissionsInfoDao = dbHelper.getUserPermissionsInfoDao();
            DeleteBuilder<UserPermissionsInfo, Integer> deleteBuilder = userPermissionsInfoDao.deleteBuilder();
            deleteBuilder.delete();

            for (Permission item : permissions) {
                UserPermissionsInfo userPermissionsInfoModel = new UserPermissionsInfo();
                userPermissionsInfoModel.setPermissionName(item.getValue());
                userPermissionsInfoDao.create(userPermissionsInfoModel);
            }

            List<UserPermissionsInfo> getUserPermissionsInfo = userPermissionsInfoDao.queryForAll();

            // افزودن اطلاعات در کلاس عمومی
            SingletonAppBase.setUserPermissionsInfo(getUserPermissionsInfo);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * نمایش خطاهای ارسال شده از سرور
     * @param errorCode کد خطا
     */
    private void ShowExceptionMessage(Integer errorCode) {
        SingletonProgressDialog.Stop(mActivity);
        Toast.makeText(SplashActivity.this, R.string.message_error_connect_server, Toast.LENGTH_LONG).show();
        if (errorCode != null) {
            Toast.makeText(SplashActivity.this, "Server Error Code: " + errorCode, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivity(intent);
        this.finish();

    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        this.finish();

    }

    @Override
    public void showMessage(String text) {

    }
}
