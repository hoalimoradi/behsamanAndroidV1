package com.ivib.splash;

/**
 * Created by ho on 7/2/2018 AD.
 */

public interface SplashView {
    void showWait();

    void removeWait();

    void startLoginActivity();

    void startMainActivity();

    void showMessage(String text);

}
