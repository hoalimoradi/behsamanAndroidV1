package com.ivib.model;

/**
 * Created by ho on 5/31/2018 AD.
 */

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GetServerErrors {
    public String FromStream(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder totalMessage = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                totalMessage.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        String errorMessage = "";
        try {
            obj = new JSONObject(totalMessage.toString());
            errorMessage = String.valueOf(obj.get("error_description").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return errorMessage;
    }
}
