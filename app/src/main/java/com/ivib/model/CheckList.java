
package com.ivib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckList {

    @SerializedName("Id")
    @Expose
    private String id;

    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("CreateDateTime")
    @Expose
    private String createDateTime;

    @SerializedName("BranchArea")
    @Expose
    private String branchArea;

    @SerializedName("BranchesManagment")
    @Expose
    private String branchesManagment;

    @SerializedName("BranchDomainName")
    @Expose
    private String branchDomainName;

    @SerializedName("BranchName")
    @Expose
    private String branchName;

    @SerializedName("BranchId")
    @Expose
    private String branchId;

    @SerializedName("BranchLatitude")
    @Expose
    private Object branchLatitude;

    @SerializedName("BranchLongitude")
    @Expose
    private Object branchLongitude;

    public CheckList(String id, String title, String createDateTime, String branchArea, String branchesManagment, String branchDomainName, String branchName, String branchId, Object branchLatitude, Object branchLongitude) {
        this.id = id;
        this.title = title;
        this.createDateTime = createDateTime;
        this.branchArea = branchArea;
        this.branchesManagment = branchesManagment;
        this.branchDomainName = branchDomainName;
        this.branchName = branchName;
        this.branchId = branchId;
        this.branchLatitude = branchLatitude;
        this.branchLongitude = branchLongitude;
    }

    public CheckList() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getBranchArea() {
        return branchArea;
    }

    public void setBranchArea(String branchArea) {
        this.branchArea = branchArea;
    }

    public String getBranchesManagment() {
        return branchesManagment;
    }

    public void setBranchesManagment(String branchesManagment) {
        this.branchesManagment = branchesManagment;
    }

    public String getBranchDomainName() {
        return branchDomainName;
    }

    public void setBranchDomainName(String branchDomainName) {
        this.branchDomainName = branchDomainName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public Object getBranchLatitude() {
        return branchLatitude;
    }

    public void setBranchLatitude(Object branchLatitude) {
        this.branchLatitude = branchLatitude;
    }

    public Object getBranchLongitude() {
        return branchLongitude;
    }

    public void setBranchLongitude(Object branchLongitude) {
        this.branchLongitude = branchLongitude;
    }

}
