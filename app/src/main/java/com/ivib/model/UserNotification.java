
package com.ivib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserNotification {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Content")
    @Expose
    private String content;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("SenderId")
    @Expose
    private String senderId;
    @SerializedName("SenderFullName")
    @Expose
    private String senderFullName;
    @SerializedName("SenderUserName")
    @Expose
    private String senderUserName;
    @SerializedName("SenderPic")
    @Expose
    private String senderPic;
    @SerializedName("ReceiverId")
    @Expose
    private String receiverId;
    @SerializedName("ReceiverFullName")
    @Expose
    private String receiverFullName;
    @SerializedName("ReceiverUserName")
    @Expose
    private String receiverUserName;
    @SerializedName("ReceiverPic")
    @Expose
    private String receiverPic;
    @SerializedName("SendOrReceive")
    @Expose
    private Boolean sendOrReceive;
    @SerializedName("IsView")
    @Expose
    private Boolean isView;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderFullName() {
        return senderFullName;
    }

    public void setSenderFullName(String senderFullName) {
        this.senderFullName = senderFullName;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }

    public String getSenderPic() {
        return senderPic;
    }

    public void setSenderPic(String senderPic) {
        this.senderPic = senderPic;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverFullName() {
        return receiverFullName;
    }

    public void setReceiverFullName(String receiverFullName) {
        this.receiverFullName = receiverFullName;
    }

    public String getReceiverUserName() {
        return receiverUserName;
    }

    public void setReceiverUserName(String receiverUserName) {
        this.receiverUserName = receiverUserName;
    }

    public String getReceiverPic() {
        return receiverPic;
    }

    public void setReceiverPic(String receiverPic) {
        this.receiverPic = receiverPic;
    }

    public Boolean getSendOrReceive() {
        return sendOrReceive;
    }

    public void setSendOrReceive(Boolean sendOrReceive) {
        this.sendOrReceive = sendOrReceive;
    }

    public Boolean getIsView() {
        return isView;
    }

    public void setIsView(Boolean isView) {
        this.isView = isView;
    }

}
