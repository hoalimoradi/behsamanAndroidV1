package com.ivib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ho on 6/8/2018 AD.
 */



public class LatestStatusResponse {
    @SerializedName("Id")
    @Expose
    private String Id;


    @SerializedName("InputDateTime")
    @Expose
    private String InputDateTime;



    @SerializedName("OutputDateTime")
    @Expose
    private String OutputDateTime;



    @SerializedName("LatitudeInput")
    @Expose
    private Double LatitudeInput;



    @SerializedName("LongitudeInput")
    @Expose
    private Double LongitudeInput;


    @SerializedName("LatitudeOutput")
    @Expose
    private Double LatitudeOutput;

    @SerializedName("LongitudeOutput")
    @Expose
    private Double LongitudeOutput;

    @SerializedName("BranchId")
    @Expose
    private String BranchId;

    @SerializedName("BranchArea")
    @Expose
    private String BranchArea;

    @SerializedName("BranchesManagenemt")
    @Expose
    private String BranchesManagenemt;


    @SerializedName("BranchDomainName")
    @Expose
    private String BranchDomainName;

    @SerializedName("BranchName")
    @Expose
    private String BranchName;

    @SerializedName("BranchAddress")
    @Expose
    private String BranchAddress;


    @SerializedName("BranchPostalCode")
    @Expose
    private String BranchPostalCode;


    @SerializedName("ApplicationUserId")
    @Expose
    private String ApplicationUserId;



    @SerializedName("UserName")
    @Expose
    private String UserName;



    @SerializedName("UserFullName")
    @Expose
    private String UserFullName;


    public LatestStatusResponse() {
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getInputDateTime() {
        return InputDateTime;
    }

    public void setInputDateTime(String inputDateTime) {
        InputDateTime = inputDateTime;
    }

    public String getOutputDateTime() {
        return OutputDateTime;
    }

    public void setOutputDateTime(String outputDateTime) {
        OutputDateTime = outputDateTime;
    }

    public Double getLatitudeInput() {
        return LatitudeInput;
    }

    public void setLatitudeInput(Double latitudeInput) {
        LatitudeInput = latitudeInput;
    }

    public Double getLongitudeInput() {
        return LongitudeInput;
    }

    public void setLongitudeInput(Double longitudeInput) {
        LongitudeInput = longitudeInput;
    }

    public Double getLatitudeOutput() {
        return LatitudeOutput;
    }

    public void setLatitudeOutput(Double latitudeOutput) {
        LatitudeOutput = latitudeOutput;
    }

    public Double getLongitudeOutput() {
        return LongitudeOutput;
    }

    public void setLongitudeOutput(Double longitudeOutput) {
        LongitudeOutput = longitudeOutput;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getBranchArea() {
        return BranchArea;
    }

    public void setBranchArea(String branchArea) {
        BranchArea = branchArea;
    }

    public String getBranchesManagenemt() {
        return BranchesManagenemt;
    }

    public void setBranchesManagenemt(String branchesManagenemt) {
        BranchesManagenemt = branchesManagenemt;
    }

    public String getBranchDomainName() {
        return BranchDomainName;
    }

    public void setBranchDomainName(String branchDomainName) {
        BranchDomainName = branchDomainName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBranchAddress() {
        return BranchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        BranchAddress = branchAddress;
    }

    public String getBranchPostalCode() {
        return BranchPostalCode;
    }

    public void setBranchPostalCode(String branchPostalCode) {
        BranchPostalCode = branchPostalCode;
    }

    public String getApplicationUserId() {
        return ApplicationUserId;
    }

    public void setApplicationUserId(String applicationUserId) {
        ApplicationUserId = applicationUserId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserFullName() {
        return UserFullName;
    }

    public void setUserFullName(String userFullName) {
        UserFullName = userFullName;
    }
}
