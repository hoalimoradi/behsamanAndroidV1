package com.ivib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ho on 6/15/2018 AD.
 */

public class Token {


    @SerializedName("access_token")
    @Expose
    public String access_token;


    @SerializedName("token_type")
    @Expose
    public String token_type;


    @SerializedName("expires_in")
    @Expose
    public String expires_in;



    @SerializedName("userName")
    @Expose
    public String userName;



    @SerializedName(".issued")
    @Expose
    public String issued;


    @SerializedName(".expires")
    @Expose
    public String expires;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }
}
