package com.ivib.model.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AnswerList implements Serializable {

    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("ConditionalQuestionsViewModels")
    @Expose
    private List<ConditionalQuestions> ConditionalQuestions;

    @SerializedName("Point")
    @Expose
    private Integer point;

    @SerializedName("Id")
    @Expose
    private String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<com.ivib.model.Question.ConditionalQuestions> getConditionalQuestions() {
        return ConditionalQuestions;
    }

    public void setConditionalQuestions(List<com.ivib.model.Question.ConditionalQuestions> conditionalQuestions) {
        ConditionalQuestions = conditionalQuestions;
    }
}
