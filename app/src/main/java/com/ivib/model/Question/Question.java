package com.ivib.model.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {

    @SerializedName("DefaultAnswerValue")
    @Expose
    private String DefaultAnswerValue;

    @SerializedName("DefaultAnswerChoiseId")
    @Expose
    private String DefaultAnswerChoiseId;

    @SerializedName("Id")
    @Expose
    private String id;

    @SerializedName("Title")
    @Expose
    private String title;

    @SerializedName("IsAnswerValue")
    @Expose
    private Boolean IsAnswerValue;

    @SerializedName("Ratio")
    @Expose
    private Integer ratio;

    @SerializedName("QuestionDocumentStatus")
    @Expose
    private int QuestionDocumentStatus;

    @SerializedName("QuestionDocumentDescription")
    @Expose
    private int QuestionDocumentDescription;

    @SerializedName("Order")
    @Expose
    private Integer order;

    @SerializedName("IsDisable")
    @Expose
    private boolean isDisable;

    @SerializedName("AnswerList")
    @Expose
    private List<AnswerList> answerList = null;

    @SerializedName("QuestionGroupId")
    @Expose
    private String questionGroupId;

    @SerializedName("QuestionGroupName")
    @Expose
    private String QuestionGroupName;

    @SerializedName("Description")
    @Expose
    private String Description;

    @SerializedName("StandardQuestionFile")
    @Expose
    private String StandardQuestionFile;

    public String getDefaultAnswerValue() {
        return DefaultAnswerValue;
    }

    public void setDefaultAnswerValue(String defaultAnswerValue) {
        DefaultAnswerValue = defaultAnswerValue;
    }

    public String getDefaultAnswerChoiseId() {
        return DefaultAnswerChoiseId;
    }

    public void setDefaultAnswerChoiseId(String defaultAnswerChoiseId) {
        DefaultAnswerChoiseId = defaultAnswerChoiseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRatio() {
        return ratio;
    }

    public void setRatio(Integer ratio) {
        this.ratio = ratio;
    }

    public int getQuestionDocumentStatus() {
        return QuestionDocumentStatus;
    }

    public void setQuestionDocumentStatus(int questionDocumentStatus) {
        QuestionDocumentStatus = questionDocumentStatus;
    }

    public int getQuestionDocumentDescription() {
        return QuestionDocumentDescription;
    }

    public void setQuestionDocumentDescription(int questionDocumentDescription) {
        QuestionDocumentDescription = questionDocumentDescription;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public boolean isDisable() {
        return isDisable;
    }

    public void setDisable(boolean disable) {
        isDisable = disable;
    }

    public List<AnswerList> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<AnswerList> answerList) {
        this.answerList = answerList;
    }

    public String getQuestionGroupId() {
        return questionGroupId;
    }

    public void setQuestionGroupId(String questionGroupId) {
        this.questionGroupId = questionGroupId;
    }

    public String getQuestionGroupName() {
        return QuestionGroupName;
    }

    public void setQuestionGroupName(String questionGroupName) {
        QuestionGroupName = questionGroupName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Boolean getAnswerValue() {
        return IsAnswerValue;
    }

    public void setAnswerValue(Boolean answerValue) {
        IsAnswerValue = answerValue;
    }

    public String getStandardQuestionFile() {
        return StandardQuestionFile;
    }

    public void setStandardQuestionFile(String standardQuestionFile) {
        StandardQuestionFile = standardQuestionFile;
    }

    @Override
    public String toString() {
        return getOrder() + "   " + getId() + "   "+ getTitle();
    }
}
