package com.ivib.model.Question;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ConditionalQuestions implements Serializable {
    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    @SerializedName("QuestionId")
    @Expose
    private String QuestionId;

}
