package com.ivib.model;

/**
 * Created by ho on 7/16/2018 AD.
 */

public class LastUploadStatusObject {
    private String checkListId;
    private int total;
    private int remain;

    public LastUploadStatusObject(String checkListId, int total, int remain) {
        this.checkListId = checkListId;
        this.total = total;
        this.remain = remain;
    }

    public String getCheckListId() {
        return checkListId;
    }

    public void setCheckListId(String checkListId) {
        this.checkListId = checkListId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }
}
