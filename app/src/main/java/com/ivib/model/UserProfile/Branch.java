
package com.ivib.model.UserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Branch {

    @SerializedName("Area")
    @Expose
    private String area;


    @SerializedName("BranchesManagment")
    @Expose
    private String branchesManagment;


    @SerializedName("DomainName")
    @Expose
    private String domainName;


    @SerializedName("BranchName")
    @Expose
    private String branchName;


    @SerializedName("Id")
    @Expose
    private String id;


    @SerializedName("Checked")
    @Expose
    private Boolean checked;

    @SerializedName("Address")
    @Expose
    private String Address;

    @SerializedName("PostalCode")
    @Expose
    private String PostalCode;


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBranchesManagment() {
        return branchesManagment;
    }

    public void setBranchesManagment(String branchesManagment) {
        this.branchesManagment = branchesManagment;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }


    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }
}
