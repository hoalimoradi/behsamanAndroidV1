package com.ivib.model.UserProfile;

/**
 * Created by ho on 5/31/2018 AD.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class UserProfile {

    @SerializedName("AppVersionAndroid")
    @Expose
    private int AppVersionAndroid;


    @SerializedName("NewNotificationCount")
    @Expose
    private Integer newNotificationCount;
    @SerializedName("Roles")
    @Expose
    private List<Role> roles = null;
    @SerializedName("Permissions")
    @Expose
    private List<Permission> permissions = null;
    @SerializedName("Branchs")
    @Expose
    private List<Branch> branchs = null;
    @SerializedName("Images")
    @Expose
    private List<Image> images = null;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("LockoutEnabled")
    @Expose
    private Boolean lockoutEnabled;
    @SerializedName("ImageLastAddress")
    @Expose
    private String imageLastAddress;
    @SerializedName("IsHaveRoles")
    @Expose
    private Boolean isHaveRoles;
    @SerializedName("CountRoles")
    @Expose
    private Integer countRoles;
    @SerializedName("IsHaveBranches")
    @Expose
    private Boolean isHaveBranches;
    @SerializedName("CountBranches")
    @Expose
    private Integer countBranches;
    @SerializedName("IsHavePics")
    @Expose
    private Boolean isHavePics;
    @SerializedName("CountPics")
    @Expose
    private Integer countPics;

    public Integer getNewNotificationCount() {
        return newNotificationCount;
    }

    public void setNewNotificationCount(Integer newNotificationCount) {
        this.newNotificationCount = newNotificationCount;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<Branch> getBranchs() {
        return branchs;
    }

    public void setBranchs(List<Branch> branchs) {
        this.branchs = branchs;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Boolean getLockoutEnabled() {
        return lockoutEnabled;
    }

    public void setLockoutEnabled(Boolean lockoutEnabled) {
        this.lockoutEnabled = lockoutEnabled;
    }

    public String getImageLastAddress() {
        return imageLastAddress;
    }

    public void setImageLastAddress(String imageLastAddress) {
        this.imageLastAddress = imageLastAddress;
    }

    public Boolean getIsHaveRoles() {
        return isHaveRoles;
    }

    public void setIsHaveRoles(Boolean isHaveRoles) {
        this.isHaveRoles = isHaveRoles;
    }

    public Integer getCountRoles() {
        return countRoles;
    }

    public void setCountRoles(Integer countRoles) {
        this.countRoles = countRoles;
    }

    public Boolean getIsHaveBranches() {
        return isHaveBranches;
    }

    public void setIsHaveBranches(Boolean isHaveBranches) {
        this.isHaveBranches = isHaveBranches;
    }

    public Integer getCountBranches() {
        return countBranches;
    }

    public void setCountBranches(Integer countBranches) {
        this.countBranches = countBranches;
    }

    public Boolean getIsHavePics() {
        return isHavePics;
    }

    public void setIsHavePics(Boolean isHavePics) {
        this.isHavePics = isHavePics;
    }

    public Integer getCountPics() {
        return countPics;
    }

    public void setCountPics(Integer countPics) {
        this.countPics = countPics;
    }

    public int getAppVersionAndroid() {
        return AppVersionAndroid;
    }

    public void setAppVersionAndroid(int appVersionAndroid) {
        AppVersionAndroid = appVersionAndroid;
    }
}
