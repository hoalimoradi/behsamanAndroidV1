package com.ivib.model;

/**
 * Created by ho on 5/31/2018 AD.
 */

public class LanguagesEnum {

    public enum Languages
    {
        English(45),
        Persian(83),
        PersianIran(84);

        Languages (int i)
        {
            this.type = i;
        }

        private int type;

        public int getNumericType()
        {
            return type;
        }
    }
}
