package com.ivib.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AttachFile implements Serializable {

    @SerializedName("Id")
    @Expose
    private int Id;

    @SerializedName("CheckListId")
    @Expose
    private String CheckListId;

    @SerializedName("QuestionId")
    @Expose
    private String QuestionId;

    @SerializedName("UrlString")
    @Expose
    private String UrlString;

    @SerializedName("IsLocal")
    @Expose
    private boolean IsLocal;

    public AttachFile(int id, String checkListId, String questionId, String urlString, boolean isLocal) {
        Id = id;
        CheckListId = checkListId;
        QuestionId = questionId;
        UrlString = urlString;
        IsLocal = isLocal;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCheckListId() {
        return CheckListId;
    }

    public void setCheckListId(String checkListId) {
        CheckListId = checkListId;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getUrlString() {
        return UrlString;
    }

    public void setUrlString(String urlString) {
        UrlString = urlString;
    }

    public boolean isLocal() {
        return IsLocal;
    }

    public void setLocal(boolean local) {
        IsLocal = local;
    }

}
