package com.ivib.userCheckLists;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ivib.R;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.model.CheckList;
import com.ivib.model.Group;
import com.ivib.model.Question.Question;
import com.ivib.questionGroup.QuestionGroupActivity;
import com.ivib.questionsByGroupid.QuestionsByGroupidActivity;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class UserCheckListActivity extends AppCompatActivity implements UserCheckListView{

    public String checkListIdClicked = "x";
    private RecyclerView list;
    private LinearLayout retryLinearLayout , empty_LinearLayout ;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progress;
    private UserCheckListPresenter presenter;
    private UserCheckListView view;
    private TextView mTitle;
    public SessionManager session;
    private static Animation shakeAnimation;
    private LinearLayout content_layout;
    public int previousPercent =0 ;
    public Handler handler;
    public  Runnable runnable;
    int delay = 2*1000; //1 second=1000 milisecond, 15*1000=15seconds
    private Activity mActivity;
    public   UserCheckListAdapter adapter;
    public int sendPosition =0;
    public boolean sendBottunIsClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_check_list);
        mActivity=this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handler = new Handler();


        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        content_layout = findViewById(R.id.content_layout);
        session = new SessionManager(getApplicationContext());
        mTitle = findViewById(R.id.mtitle);
        list = findViewById(R.id.list);
        empty_LinearLayout = findViewById(R.id.empty_LinearLayout);
        retryLinearLayout = findViewById(R.id.retryLinearLayout);
        progress = findViewById(R.id.progress);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.material_red_200, R.color.material_red_300);
        swipeRefreshLayout.setBackgroundColor(Color.TRANSPARENT);
        view = this;
        presenter = new UserCheckListPresenter(this,view);

        // دریافت اطلاعات چک لیست ها
        presenter.getCheckLists();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clearList();
                        presenter.getCheckLists();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

    @Override
    protected void onResume() {
        //start handler as activity become visible

        handler.postDelayed( runnable = new Runnable() {
            public void run() {
                Log.e("  onResume "," اجرای تایمر" + delay);
                if (previousPercent ==  presenter.getLastUploadStatus(checkListIdClicked)){

                }else {
                    if (adapter != null){
                        previousPercent = presenter.getLastUploadStatus(checkListIdClicked);
                        adapter.updateProgressBarValue(sendPosition,previousPercent);
                        Log.e("  onResume ","sendPosition    اداپتر موجوده" + sendPosition + " previousPercent  " + previousPercent);

                    }else {
                        Log.e("  onResume "," اداپتر ناله" );

                    }
                }
                //do something

                handler.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.back_menu)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (sendBottunIsClicked == true && presenter.getLastUploadStatus(checkListIdClicked) != 100 ){
            showMessage("تا کامل شدن ارسال فایل ها صبر کنید");
        }else {
            super.onBackPressed();
            overridePendingTransitionExit();
        }

    }

    private void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {
        empty_LinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String text) {
        content_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_layout, (text));
    }

    @Override
    public void hideEmpty() {
        empty_LinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void retry() {
        retryLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryLinearLayout.setVisibility(View.GONE);
    }

    /**
     * پر کردن آداپتر لیست چک لیست ها
     * @param checkLists لیست چک لیست ها
     */
    @Override
    public void getUserCheckList(final List<CheckList> checkLists) {

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);

        // آداپتر لیست چک لیست ها
        adapter = new UserCheckListAdapter(getApplicationContext(),checkLists,
                new UserCheckListAdapter.OnItemClickListener() {

                    /**
                     * کلیک بر روی دکمه ویرایش
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickEdit(View v, int position) {

                        // دریافت سوالات چک لیست از پایگاه داده
                        List<Question> qListFromDB =  presenter.getQListFromDB(checkLists.get(position).getId());

                        // یافتن گروه های سوالات
                        List<Group> groupList =new ArrayList<>();
                        List<String> sList = new ArrayList<>();
                        for (Question q: qListFromDB) {
                            Group group = new Group();
                            group.setId(q.getQuestionGroupId());
                            group.setName(q.getQuestionGroupName());

                            if ( ! sList.contains(q.getQuestionGroupId())){
                                groupList.add(group);
                                sList.add(q.getQuestionGroupId());
                            }
                        }

                        // پر کردن لیست گروه ها و سوالات در کلاس عمومی
                        GlobalClass globalVariable  = (GlobalClass)getApplication().getApplicationContext() ;
                        globalVariable.setGlobalQL(qListFromDB);
                        globalVariable.setGlobalQGL(groupList);
                        globalVariable.setBranchId(checkLists.get(position).getBranchId());
                        checkListIdClicked =   checkLists.get(position).getId();
                        CheckListInfo checkListInfo = presenter.getCheckListInfoFromDB(checkLists.get(position).getId());
                        globalVariable.setCheckListInfoId(checkListInfo.getId());


                        //ست کردن کلاس والد جهت برگشت و رفتن به صفحه لیست گروه سوالات
                        Intent intent = new Intent(getApplicationContext(), QuestionGroupActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("parent", "UserCheckList");
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                    /**
                     * کلیک بر روی دکمه ارسال
                     * @param v ویو
                     * @param position موقعیت
                     */
                    @Override
                    public void onClickSend(View v, int position) {
                        // دریافت سوالات چک لیست از پایگاه داده
                        List<Question> qListFromDB =  presenter.getQListFromDB(checkLists.get(position).getId());

                        // یافتن گروه های سوالات
                        List<Group> groupList =new ArrayList<>();
                        List<String> sList = new ArrayList<>();
                        for (Question q: qListFromDB) {
                            Group group = new Group();
                            group.setId(q.getQuestionGroupId());
                            group.setName(q.getQuestionGroupName());

                            if ( ! sList.contains(q.getQuestionGroupId())){
                                groupList.add(group);
                                sList.add(q.getQuestionGroupId());
                            }
                        }

                        // پر کردن لیست گروه ها و سوالات در کلاس عمومی
                        GlobalClass globalVariable  = (GlobalClass)getApplication().getApplicationContext() ;
                        globalVariable.setGlobalQL(qListFromDB);
                        globalVariable.setGlobalQGL(groupList);
                        globalVariable.setBranchId(checkLists.get(position).getBranchId());
                        checkListIdClicked =   checkLists.get(position).getId();
                        CheckListInfo checkListInfo = presenter.getCheckListInfoFromDB(checkLists.get(position).getId());
                        globalVariable.setCheckListInfoId(checkListInfo.getId());


                        sendBottunIsClicked = true;
                        SingletonProgressDialog.Start(UserCheckListActivity.this," ",0);

                        checkListIdClicked =   checkLists.get(position).getId();
                        // ارسال چک لیست
                        sendPosition = position;
                        presenter.sendCheckList(checkLists.get(position).getId());
                        checkListIdClicked =   checkLists.get(position).getId();
                        presenter.getLastUploadStatus(checkLists.get(position).getId());


                    }

                });
        list.setAdapter(adapter);
    }

    @Override
    public void clearList() {

        List<CheckList> data = new ArrayList<>();
        UserCheckListAdapter adapter = new UserCheckListAdapter(getApplicationContext(),data,
                new UserCheckListAdapter.OnItemClickListener() {
                    @Override
                    public void onClickEdit(View v, int position) {
                    }

                    @Override
                    public void onClickSend(View v, int position) {
                    }


                });
        list.setAdapter(adapter);
    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
        list.setAdapter(null);
    }
}
