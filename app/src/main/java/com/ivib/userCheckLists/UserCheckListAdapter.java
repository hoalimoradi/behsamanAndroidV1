package com.ivib.userCheckLists;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.ivib.R;
import com.ivib.model.CheckList;
import com.ivib.utility.AutofitText.AutofitTextView;
import com.ivib.utility.CircleProgressBar;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.Slidr;

import java.util.List;

public class UserCheckListAdapter extends RecyclerView.Adapter<UserCheckListAdapter.ViewHolder> {

    private final UserCheckListAdapter.OnItemClickListener listener;
    private List<CheckList> data;
    private Context context;
    public int[] percent;

    UserCheckListAdapter(Context context, List<CheckList> data, UserCheckListAdapter.OnItemClickListener listener) {
        this.listener = listener;
        this.data = data;
        this.context = context;
        if ( !data.isEmpty()) percent = new int[data.size()];
    }

    @Override
    public UserCheckListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_user_check_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        FontChangeCrawler fontChanger = new FontChangeCrawler(view.getContext().getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) view);
        return new UserCheckListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserCheckListAdapter.ViewHolder holder, int position) {

        holder.BranchesManagment.setText(data.get(position).getBranchesManagment());
        holder.DomainName.setText(data.get(position).getBranchDomainName());
        holder.BranchName.setText(data.get(position).getBranchName());
        holder.Area.setText(data.get(position).getBranchArea());
        holder.Title.setText(data.get(position).getTitle());
        holder.CreateDateTime.setText(data.get(position).getCreateDateTime());



    }

    public void updateProgressBarValue(int position, int perce){
        percent[position] = perce;
        Log.e("","updateProgressBarValue");

    }

    @Override
    public int getItemCount() {

        return  data == null ? 0 : data.size();
    }

    public interface OnItemClickListener {
        void onClickEdit(View v, int position);
        void onClickSend(View v, int position);

    }
    class ViewHolder extends RecyclerView.ViewHolder {
        AutofitTextView BranchesManagment;
        AutofitTextView DomainName;
        AutofitTextView BranchName;
        AutofitTextView Area;
        AutofitTextView Title;
        AutofitTextView CreateDateTime;
        CircleProgressBar line_progress;
        LinearLayout slidetLinearLayout, sendLinearLayout, editLinearLayout;
        ViewHolder(View itemView) {
            super(itemView);
            BranchesManagment = itemView.findViewById(R.id.BranchesManagment);
            DomainName = itemView.findViewById(R.id.DomainName);
            BranchName = itemView.findViewById(R.id.BranchName);
            Area = itemView.findViewById(R.id.Area);
            Title = itemView.findViewById(R.id.Title);
            CreateDateTime = itemView.findViewById(R.id.CreateDateTime);
            line_progress = itemView.findViewById(R.id.line_progress);
            slidetLinearLayout = itemView.findViewById(R.id.slidetLinearLayout);
            sendLinearLayout = itemView.findViewById(R.id.sendLinearLayout);
            editLinearLayout = itemView.findViewById(R.id.editLinearLayout);
            slidetLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            sendLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickSend(v, getAdapterPosition());
                    line_progress.setProgress(percent[getAdapterPosition()]);
                    Log.e("getAdapterPosition()","  sendLinearLayout "+getAdapterPosition()+ " " );
                }
            });
            // line_progress.setProgress(listener.getLastUploadStatus(getAdapterPosition()));

             editLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickEdit(v, getAdapterPosition());
                }
            });

        }
    }

}

