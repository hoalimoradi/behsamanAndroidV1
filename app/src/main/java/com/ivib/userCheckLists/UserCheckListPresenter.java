package com.ivib.userCheckLists;

import android.content.Context;
import android.util.Log;

import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.dbOperations.dbQuestionOperations;
import com.ivib.data.dbOperations.dbQuestionsFileOperations;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.data.local.Entity.QuestionsFilesInfo;
import com.ivib.data.local.Entity.QuestionsInfo;
import com.ivib.data.remote.QuestionsOperations;
import com.ivib.data.remote.UserCheckListOperations;
import com.ivib.model.CheckList;
import com.ivib.model.LastUploadStatusObject;
import com.ivib.model.Question.Question;
import com.ivib.utility.ConvertClass;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.ProgressRequestBody;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserCheckListPresenter {
    private static final String TAG = "UserCheckListPresenter";

    private final Context context;
    private final UserCheckListView view;


    private List<LastUploadStatusObject> lastUploadStatusObjects = new ArrayList<>();

    private CheckListInfo CheckListInfo;
    private List<Question> QuestionsAnswer;
    private List<QuestionsInfo> SendQuestionsList;
    private List<QuestionsFilesInfo> SendQuestionsFilesList;

    UserCheckListPresenter(Context context, UserCheckListView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت لیست چک لیست ها
     */
    public void getCheckLists() {
        view.showWait();
        view.hideEmpty();
        view.hideRetry();

        // فراخوانی سرویس لیست چک لیست ها
        final Call<List<CheckList>> createdRequest = new UserCheckListOperations().GetService().GetCheckLists();
        createdRequest.enqueue(new Callback<List<CheckList>>() {

            @Override
            public void onResponse(Call<List<CheckList>> call, Response<List<CheckList>> response) {
                if (response.isSuccessful()) {
                    if (response.body().isEmpty()) {
                        view.showEmpty();
                    } else {
                        // بررسی وجود چک لیست ها در پایگاه داده
                        CheckInExistInDb(response.body());

                        // نمایش لیست چک لیست ها در ویو
                        view.getUserCheckList(response.body());
                    }
                    view.removeWait();
                } else {
                    view.removeWait();
                    view.showEmpty();
                    view.hideRetry();
                    view.showMessage("هیچ چک لیستی جهت نمایش وجود ندارد!");
                }
            }

            @Override
            public void onFailure(Call<List<CheckList>> call, Throwable throwable) {
                view.removeWait();
                view.retry();
                view.hideEmpty();
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * دریافت سوالات پاسخ داده شده یک چک لیست
     *
     * @param checkListId شناسه چک لیست
     * @return List<Question> لیستی از سوالات
     */
    public List<Question> getQListFromDB(String checkListId) {
        // دریافت سوالات پاسخ داده شده یک چک لیست
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        CheckListInfo checkListInfo = dbCheckListOp.GetByCheckListId(checkListId);

        // تبدیل بایت به مدل لیست سوالات
        return ConvertClass.ToModelGlobalData(checkListInfo.GlobalData);
    }



    public CheckListInfo getCheckListInfoFromDB(String checkListId) {

        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        CheckListInfo checkListInfo = dbCheckListOp.GetByCheckListId(checkListId);
        return checkListInfo;
    }

    /**
     * بررسی وجود چک لیست در پایگاه داده
     *
     * @param input لیست چک لیست ها
     */
    private void CheckInExistInDb(List<CheckList> input) {
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);

        // برای هر چک لیست دریافت شده از سرور بررسی می کند که اگر در پایگاه داده وجود ندارد آن را اضافه نماید
        for (CheckList item : input) {
            if (dbCheckListOp.IsNotExistByCheckListId(item.getId())) {
                // دریافت سوالات از چک لیست ارسال شده
                GetQuestionFromCheckList(item);
            }
        }
    }

    /**
     * دریافت سوالات از چک لیست
     *
     * @param input مدل چک لیست
     */
    private void GetQuestionFromCheckList(final CheckList input) {

        // فراخوانی سوالات یک چک لیست با توجه به شناسه چک لیست
        final Call<List<Question>> createdRequest = new QuestionsOperations().GetService().GetAllQuestion(input.getId());
        createdRequest.enqueue(new Callback<List<Question>>() {

            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                if (response.isSuccessful()) {
                    // افزودن اطلاعات چک لیست در پایگاه داده
                    AddCheckListInDb(input, response.body());
                } else {
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * افزودن چک لیست در پایگاه داده
     *
     * @param input مدل چک لیست
     * @param qList لیست سوالات
     */
    private void AddCheckListInDb(CheckList input, List<Question> qList) {
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);

        // تبدیل لیست سوالات به بایت
        byte[] globalData = ConvertClass.ToByteGlobalData(qList);

        // ساخت مدل پایگاه داده
        CheckListInfo entity = new CheckListInfo(0, input.getId(), input.getBranchId(), input.getTitle(), input.getCreateDateTime(), input.getBranchArea(), input.getBranchesManagment(), input.getBranchDomainName(), input.getBranchName(), globalData);

        // افزودن چک لیست جدید در پایگاه داده
        dbCheckListOp.Add(entity);
    }

    /**
     * دریافت آخرین وضعیت آپلود یک چک لیست
     *
     * @return int عدد وضعیت
     */
    public int getLastUploadStatus(String checkListId) {
        if (checkListId == "x")
            return  0;
        // دریافت چک لیست از پایگاه داده
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        CheckListInfo checkListInfo = dbCheckListOp.GetByCheckListId(checkListId);

        if (checkListInfo != null) {
            // دریافت کل سوالات پاسخ یا تغییر داده شده از پایگاه داده سوالات
            dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
            List<QuestionsInfo> questionsInfoList = dbQuestionOp.GetAll(CheckListInfo.Id);
            if (questionsInfoList != null && !questionsInfoList.isEmpty()){

                boolean isNotExitCeckList = false;
                for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                    if (item.getCheckListId().equals(checkListId)) {
                        item.setTotal(1);
                        item.setRemain(0);
                        isNotExitCeckList = true;
                        break;
                    }
                }

                if (isNotExitCeckList == false){
                    LastUploadStatusObject uploadStatusObject = new LastUploadStatusObject(checkListId,1,0);
                    lastUploadStatusObjects.add(uploadStatusObject);
                }
            }

            dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
            List<QuestionsFilesInfo> questionsFilesInfo = dbQuestionsFileOp.GetAll(checkListInfo.Id);

            if (questionsFilesInfo != null && !questionsFilesInfo.isEmpty()) {
                Log.e(TAG,"   questionsFilesInfo.size()  " + questionsFilesInfo.size());


            }
            LastUploadStatusObject result = new LastUploadStatusObject(checkListId,1,0);

            boolean isNotExitCeckList = false;
            for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                if (item.getCheckListId().equals( checkListId)){
                   result.setRemain(item.getRemain());
                   result.setTotal(item.getTotal());
                    isNotExitCeckList = true;
                    break;
                }
            }
            if (isNotExitCeckList == false){
                // بار اولی که توتال ست می شه
                LastUploadStatusObject uploadStatusObject = new LastUploadStatusObject(checkListId, questionsFilesInfo.size() + 1,0);
                lastUploadStatusObjects.add(uploadStatusObject);

            }

            return (result.getRemain() * 100) / result.getTotal();

        }else
            return 100;

    }

    /**
     * ارسال یک چک لیست
     *
     * @param checkListId شناسه چک لیست
     */
    public void sendCheckList(String checkListId) {

        // دریافت چک لیست از پایگاه داده
        dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
        CheckListInfo checkListInfo = dbCheckListOp.GetByCheckListId(checkListId);
        Log.e(TAG," checkListId "+ checkListId);
        // بررسی وجود چک لیست
        if (checkListInfo != null) {


            // دریافت مدل سوالات و جواب ها
            CheckListInfo = checkListInfo;
            QuestionsAnswer = ConvertClass.ToModelGlobalData(checkListInfo.GlobalData);

            // دریافت کل سوالات پاسخ یا تغییر داده شده از پایگاه داده سوالات
            dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
            SendQuestionsList = dbQuestionOp.GetAll(CheckListInfo.Id);





            // دریافت کل فایل های انتخاب شده برای ارسال سوالات
            dbQuestionsFileOperations dbQuestionsFileOp = new dbQuestionsFileOperations(context);
            List<QuestionsFilesInfo> questionsFilesInfo = dbQuestionsFileOp.GetAll(CheckListInfo.Id);
            SendQuestionsFilesList = questionsFilesInfo;


            //TODO check

            // بررسی وجود فایل جهت ارسال - عدد کلی = ارسال همه سوالات (1) + ارسال همه فایل ها (تعداد فایل ها جهت ارسال)
            if (questionsFilesInfo != null && !questionsFilesInfo.isEmpty()) {
                Log.e(TAG,"   questionsFilesInfo.size()  " + questionsFilesInfo.size());
                for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                    if (item.getCheckListId() == checkListId){
                        item.setTotal(questionsFilesInfo.size() + 1);
                        break;
                    }
                }

            } else {
                Log.e(TAG,"  TotalUploadFile = 1 ");
                for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                    if (item.getCheckListId() == checkListId){
                        item.setTotal( 1);
                        break;
                    }
                }
            }

            Log.e(TAG,"  شروع ارسال پاسخ سوالات ");
            // شروع ارسال پاسخ سوالات
            SendQuestions(checkListId);
        }
    }

    /**
     * ارسال تک به تک سوالات
     */
    private void SendQuestions(final String checkListId) {

        if (SendQuestionsList.isEmpty()) {
            Log.e(TAG," if ,  SendQuestionsList.isEmpty()  ");
            for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                if (item.getCheckListId() == checkListId){
                    item.setRemain(item.getRemain() + 1);
                    break;
                }
            }

            SendQuestionFiles(checkListId);
        } else {

            final QuestionsInfo model = SendQuestionsList.get(0);
            String defaultAnswerValue = "";
            String defaultAnswerChoiceId = "";

            for (Question q : QuestionsAnswer) {
                if (q.getId().equals(model.QuestionId)) {
                    Log.e(TAG," if in for ,  q.getDefaultAnswerValue() " + q.getDefaultAnswerValue());
                    defaultAnswerValue = q.getDefaultAnswerValue();
                    defaultAnswerChoiceId = q.getDefaultAnswerChoiseId();
                    break;
                }
            }

            //TODO: kkkkkkkkkkklkkkkkkkkkk
            GlobalClass globalVariable  = (GlobalClass)context.getApplicationContext() ;
            List<String> globalCQL = globalVariable.getGlobalCQL();

            globalVariable.updateGlobalCQL();

            if ( globalCQL != null &&  !globalCQL.isEmpty() && globalCQL.contains(model.QuestionId)) {
                dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
                dbQuestionOp.Delete(model.CheckListInfoId, model.QuestionId);
                Log.e(TAG," model.QuestionId " + model.QuestionId);
                SendQuestionsList.remove(0);
                SendQuestions(checkListId);

            } else {

                // فراخوانی سرویس پاسخ یک سوال
                Log.e(TAG," فراخوانی سرویس پاسخ یک سوال ");
                final Call<Void> createdRequest = new QuestionsOperations().GetService().UpdateQuestionAnswer(CheckListInfo.CheckListId, model.QuestionId, defaultAnswerValue, defaultAnswerChoiceId);
                createdRequest.enqueue(new Callback<Void>() {

                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            Log.e(TAG," onResponse isSuccessful ");
                            dbQuestionOperations dbQuestionOp = new dbQuestionOperations(context);
                            dbQuestionOp.Delete(model.CheckListInfoId, model.QuestionId);
                            Log.e(TAG," model.QuestionId " + model.QuestionId);
                            SendQuestionsList.remove(0);

                            SendQuestions(checkListId);
                        } else {
                            Log.e(TAG," onResponse is not Successful ");
                            view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                            view.removeWait();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable throwable) {
                        Log.e(TAG," onResponse  onFailure ");
                        view.showMessage("عدم برقراری ارتباط با سرور!");
                        view.removeWait();
                    }
                });
            }

        }
    }

    /**
     * ارسال تصاویر سوالات
     */
    private void SendQuestionFiles(String checkListId) {
        // بررسی وجود فایل در لیست
        Log.e(TAG," SendQuestionFiles  بررسی وجود فایل در لیست ");

        if (SendQuestionsFilesList != null && !SendQuestionsFilesList.isEmpty()) {

            final QuestionsFilesInfo model = SendQuestionsFilesList.get(0);

            Log.e(TAG," SendImageToServer      ");
            // ارسال فایل
            SendImageToServer(model,checkListId);
        } else {

            for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                if (item.getCheckListId().equals(checkListId)){
                    item.setRemain(item.getTotal());
                    break;
                }
            }

            Log.e(TAG,"  اعلام پایان ارسال چک لیست      ");

            // اعلام پایان ارسال چک لیست
            CheckListCompleteAll();


        }
    }

    /**
     * ارسال یک تصویر به سرور
     *
     * @param model مدل فایل سوال
     */
    private void SendImageToServer(final QuestionsFilesInfo model, final String checkListId) {

        Log.v(TAG, "ارسال یک تصویر به سرور!");
        ProgressRequestBody progressRequestBody;
        File newFile;
        ProgressRequestBody.UploadCallbacks listener = new ProgressRequestBody.UploadCallbacks() {

            @Override
            public void onProgressUpdate(int percentage) {
                Log.v(TAG, "progress update " + percentage);
            }

            @Override
            public void onError() {

                Log.v(TAG, "Error!");
            }

            @Override
            public void onFinish() {

                Log.v(TAG, "Finish");
            }
        };
        newFile = new File(model.FileAddress);
        progressRequestBody = new ProgressRequestBody(newFile, listener);



        GlobalClass globalVariable  = (GlobalClass)context.getApplicationContext() ;
        globalVariable.updateGlobalCQL();
        List<String> globalCQL = globalVariable.getGlobalCQL();
        if (globalCQL != null &&  !globalCQL.isEmpty() && globalCQL.contains(model.QuestionId)){

            dbQuestionsFileOperations dbQuestionFilesOp = new dbQuestionsFileOperations(context);

            //TODO حذف فایل از داخل  گوشی model.FileAddress

            dbQuestionFilesOp.Delete(model.CheckListInfoId, model.QuestionId);
            SendQuestionsFilesList.remove(0);

            // ارسال بقیه فایل ها
            SendQuestionFiles(checkListId);

        }else {
            // فراخوانی سرویس افزودن یک فایل به یک سوال
            final Call<Void> createdRequest = new QuestionsOperations(CheckListInfo.CheckListId, model.QuestionId).GetService().AddQuestionAnswerFiles(progressRequestBody);
            createdRequest.enqueue(new Callback<Void>() {

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {

                        for (  LastUploadStatusObject item :lastUploadStatusObjects) {
                            if (item.getCheckListId().contentEquals(checkListId)){
                                item.setRemain(item.getRemain() + 1);
                                break;
                            }
                        }
                        //  notifyProssecBar(position, LastUploadStatus);


                        dbQuestionsFileOperations dbQuestionFilesOp = new dbQuestionsFileOperations(context);

                        //TODO حذف فایل از داخل  گوشی model.FileAddress

                        dbQuestionFilesOp.Delete(model.CheckListInfoId, model.QuestionId);
                        SendQuestionsFilesList.remove(0);

                        // ارسال بقیه فایل ها
                        SendQuestionFiles(checkListId);
                    } else {
                        view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                        view.removeWait();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable throwable) {
                    Log.e(TAG ," onFailure  " + throwable.toString());
                    Log.e(TAG, "  onFailure  " + throwable.getStackTrace().toString());
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                    view.removeWait();
                }
            });
        }

    }

    /**
     * اعلام اتمام ارسال کل چک لیست
     */
    private void CheckListCompleteAll() {
        final Call<Void> createdRequest = new UserCheckListOperations().GetService().CheckListCompleteAll(CheckListInfo.CheckListId);
        createdRequest.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG ," onResponse  حذف اطلاعات چک لیست از پایگاه داده  " );
                    // حذف اطلاعات چک لیست از پایگاه داده
                    dbCheckListOperations dbCheckListOp = new dbCheckListOperations(context);
                    Log.e(TAG ," CheckListInfo.Id  " + CheckListInfo.Id);

                    dbCheckListOp.Delete(CheckListInfo.Id);

                    view.showMessage("اطلاعات چک لیست به طور کامل ارسال شد.");
                    view.removeWait();
                } else {
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                    view.removeWait();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور!");
                view.removeWait();
            }
        });
    }
}





