package com.ivib.userCheckLists;

import com.ivib.model.CheckList;

import java.util.List;

public interface UserCheckListView {

    void showWait();

    void removeWait();

    void showEmpty();

    void hideEmpty();

    void retry();

    void hideRetry();

    void showMessage(String text);

    void getUserCheckList(List<CheckList> checkLists);

    void clearList();

}
