package com.ivib.main;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.ivib.InputUser.InputUserActivity;
import com.ivib.OutputUser.OutputUserActivity;
import com.ivib.R;
import com.ivib.data.dbOperations.dbCheckListOperations;
import com.ivib.data.local.Entity.CheckListInfo;
import com.ivib.data.remote.InputOutputUserOperations;
import com.ivib.data.remote.QuestionsOperations;
import com.ivib.data.remote.UserCheckListOperations;
import com.ivib.model.Group;
import com.ivib.model.LatestStatusResponse;
import com.ivib.model.Question.Question;
import com.ivib.model.UserProfile.Branch;
import com.ivib.model.UserProfile.Permission;
import com.ivib.model.UserProfile.Role;
import com.ivib.model.UserProfile.UserProfile;
import com.ivib.notification.NotificationActivity;
import com.ivib.questionGroup.QuestionGroupActivity;
import com.ivib.tempCheckList.TempCheckListActivity;
import com.ivib.userCheckLists.UserCheckListActivity;
import com.ivib.utility.ConvertClass;
import com.ivib.utility.CustomToast;
import com.ivib.utility.FontChangeCrawler;
import com.ivib.utility.GlobalClass;
import com.ivib.utility.SessionManager;
import com.ivib.utility.SingletonAppBase;
import com.ivib.utility.SingletonProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements MainView {

    private static final int REQUEST_LOCATION = 1;
    private static Animation shakeAnimation;
    private final String mainActivity_TAG = "MainActivity";
    public MainView mainView;
    public SessionManager session;
    private LinearLayout content_main_layout;
    private TextView firstNameLastName;
    private TextView role;
    private CircleImageView profile_image;
    private Context context;
    private Activity mActivity;
    private MainView view;
    private GlobalClass GlobalVariable;
    private dbCheckListOperations DBCheckListOp;
    private LocationManager locationManager;
    private LatestStatusResponse latestStatusResponse;
    private List<Branch> branchs;
    private ImageView exit_account;
    private CircleImageView ic_notifications;
    private String IS_First;
    private boolean UserCheckListPermissions = false;
    private boolean NotInPersonLoginPermissions = false;
    private boolean UserNotificationsPermissions = false;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        mActivity = this;
        SingletonAppBase.CheckBaseInfo(mActivity);

        context = getApplicationContext();
        session = new SessionManager(context);
        view = this;
        GlobalVariable = (GlobalClass) context.getApplicationContext();
        content_main_layout = findViewById(R.id.content_main_layout);
        DBCheckListOp = new dbCheckListOperations(context);
        firstNameLastName = findViewById(R.id.firstNameLastName);
        role = findViewById(R.id.role);
        profile_image = findViewById(R.id.profile_image);
        LinearLayout tempCheckListLayout = findViewById(R.id.tempCheckListLayout);
        LinearLayout newCheckListLayout = findViewById(R.id.newCheckListLayout);
        LinearLayout inputOutputUserLayout = findViewById(R.id.inputOutputUserLayout);
        LinearLayout checkListLayout = findViewById(R.id.checkListLayout);
        ImageView exit_account = findViewById(R.id.exit_account);
        ic_notifications = findViewById(R.id.ic_notifications);

        SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.material_red_200, R.color.material_red_300);
        swipeRefreshLayout.setBackgroundColor(Color.TRANSPARENT);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "IRANSansMobile.ttf");
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        MainPresenter presenter = new MainPresenter(this, this);



        // دریافت آخرین وضعیت اطلاعات کاربر
        presenter.getUserProfile();


        mainView = this;

        session = new SessionManager(context);

        final HashMap<String, String> user = session.getUserDetails();


        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

//

        ic_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWait();

                if (UserNotificationsPermissions == true) {



                    Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                    startActivity(i);
                    removeWait();
                } else {
                    showMessage("شما مجوز مشاهده پیام ها را ندارید");
                }
            }
        });


        newCheckListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showWait();

                if (UserCheckListPermissions == true) {

                    GetLatestStatus();
                    removeWait();
                } else {
                    showMessage("شما مجوز ایجاد چک لیست ندارید");
                }


            }
        });


        tempCheckListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWait();
                if (UserCheckListPermissions == true) {

                    Intent i = new Intent(getBaseContext(), TempCheckListActivity.class);
                    startActivity(i);

                    removeWait();
                } else {
                    showMessage("شما مجوز ایجاد چک لیست ندارید");
                }


            }
        });

        inputOutputUserLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NotInPersonLoginPermissions == true) {

                    showWait();
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        removeWait();
                        buildAlertMessageNoGps();

                    } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        showWait();
                        getLocation();


                        final Call<LatestStatusResponse> createdRequest = new InputOutputUserOperations().GetService().LatestStatus();
                        createdRequest.enqueue(new Callback<LatestStatusResponse>() {
                            @Override
                            public void onResponse(Call<LatestStatusResponse> call, Response<LatestStatusResponse> response) {


                                if (response.isSuccessful()) {

                                    Log.e(mainActivity_TAG, "  getLatestStatus " + response.body());
                                    if (response.body().equals(null)) {

                                        Intent i = new Intent(getBaseContext(), InputUserActivity.class);
                                        startActivity(i);

                                        removeWait();
                                        Log.e(mainActivity_TAG, "response.body().equals(null)");

                                    } else {
                                        //then the user should be log out


                                        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
                                        //Set name and email in global/application context
                                        globalVariable.setLatestStatusResponse(response.body());
                                        Intent i = new Intent(getBaseContext(), OutputUserActivity.class);
                                        startActivity(i);
                                        removeWait();
                                    }

                                } else if (response.code() == 401) {


                                    Intent i = new Intent(getBaseContext(), InputUserActivity.class);
                                    startActivity(i);
                                    Log.e(mainActivity_TAG, "response.code() == 401");
                                    removeWait();
                                    //  SingletonAppBase.ResetToken(mActivity);
                                } else {

                                    Intent i = new Intent(getBaseContext(), InputUserActivity.class);
                                    startActivity(i);
                                    Log.e(mainActivity_TAG, response.message());
                                    removeWait();
                                    // Toast.makeText(LoginActivity.this, new GetServerErrors().FromStream(response.errorBody().byteStream()), Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<LatestStatusResponse> call, Throwable throwable) {

                                removeWait();
                                Log.e(mainActivity_TAG, "  getLatestStatus " + throwable.toString());
                            }
                        });
                    }

                } else {



                    showMessage("شما مجوز تغییر وضعیت ورود و خروج را ندارید");
                }

            }
        });

        checkListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//
                if (UserCheckListPermissions == true) {
                    Intent i = new Intent(getBaseContext(), UserCheckListActivity.class);
                    //  i.putExtra("PersonID", personID);
                    startActivity(i);
                } else {
                    showMessage("شما مجوز ایجاد چک لیست ندارید");
                }


            }
        });

        exit_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog();

            }
        });


        // ساخت مدل عمومی
        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
        if (globalVariable.isTapAnimation()) {
            globalVariable.setTapAnimation(false);

            // We load a drawable and create a location to show a tap target here
            // We need the display to get the width and height at this point in time
            final Display display = getWindowManager().getDefaultDisplay();
            // Load our little droid guy
            final Drawable droid = ContextCompat.getDrawable(this, R.drawable.ic_camera_alt_24dp);
            // Tell our droid buddy where we want him to appear
            final Rect droidTarget = new Rect(0, 0, droid.getIntrinsicWidth() * 2, droid.getIntrinsicHeight() * 2);
            // Using deprecated methods makes you look way cool
            droidTarget.offset(display.getWidth() / 2, display.getHeight() / 2);

            final SpannableString sassyDesc = new SpannableString("برای هر شعبه فقط یک چک لیست را می توانید ثبت کنید");
            sassyDesc.setSpan(new StyleSpan(Typeface.ITALIC), sassyDesc.length() - "sometimes".length(), sassyDesc.length(), 0);

            // We have a sequence of targets, so lets build it!
            final TapTargetSequence sequence = new TapTargetSequence(this)
                    .targets(
                            // This tap target will target the back button, we just need to pass its containing toolbar
                            // This tap target will target our droid buddy at the given target rect
                            TapTarget.forBounds(droidTarget,
                                    "فایل های پیوست !", "برای بعضی از سوالات باید با استفاده از دوربین گوشی خود تصویر هم بفرستید")
                                    .cancelable(false)
                                    .icon(droid)
                                    .id(1)
                    )
                    .listener(new TapTargetSequence.Listener() {
                        // This listener will tell us when interesting(tm) events happen in regards
                        // to the sequence
                        @Override
                        public void onSequenceFinish() {
                            // ((TextView) findViewById(R.id.educated)).setText("Congratulations! You're educated now!");
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                            Log.d("TapTargetView", "Clicked on " + lastTarget.id());
                        }

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {
                            final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("")
                                    .setMessage("  ")
                                    .setPositiveButton("گرفتم!", null).show();
                            TapTargetView.showFor(dialog,
                                    TapTarget.forView(dialog.getButton(DialogInterface.BUTTON_POSITIVE), " اوه نه راهنمایی رو لغو کردی ", "You canceled the sequence at step " + lastTarget.id())
                                            .cancelable(false)
                                            .tintTarget(false), new TapTargetView.Listener() {
                                        @Override
                                        public void onTargetClick(TapTargetView view) {
                                            super.onTargetClick(view);
                                            dialog.dismiss();
                                        }
                                    });
                        }
                    });

            // You don't always need a sequence, and for that there's a single time tap target
            final SpannableString spannedDesc = new SpannableString(" به صفحه ورود خواهید رفت ");
            // spannedDesc.setSpan(new UnderlineSpan(), spannedDesc.length() - "TapTargetView".length(), spannedDesc.length(), 0);
            TapTargetView.showFor(this, TapTarget.forView(findViewById(R.id.exit_account), "  دکمه خروج از حساب کاربری ", spannedDesc)
                    .cancelable(true)
                    .drawShadow(true)
                    .dimColor(R.color.yellow)
                    .outerCircleColor(R.color.colorAccent)
                    .targetCircleColor(R.color.colorPrimary)
                    .transparentTarget(true)
                    .textColor(android.R.color.black)
                    .dimColor(R.color.gray)
                    // .titleTextDimen(R.dimen.title_text_size)
                    .tintTarget(false), new TapTargetView.Listener() {
                @Override
                public void onTargetClick(TapTargetView view) {
                    super.onTargetClick(view);
                    // .. which evidently starts the sequence we defined earlier
                    sequence.start();
                }

                @Override
                public void onOuterCircleClick(TapTargetView view) {
                    super.onOuterCircleClick(view);

                }

                @Override
                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                    Log.d("TapTargetViewSample", "You dismissed me :(");
                }
            });
        }

        // بررسی وجود مجوز لوکیشن
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        // کلیک بر روی دکمه نوتیفیکیشن
        ic_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // رفتن به صفحه نوتیفیکیشن ها
                Intent i = new Intent(getBaseContext(), NotificationActivity.class);
                startActivity(i);
            }
        });

        // کلیک بر روی دکمه ثبت چک لیست جدید
        newCheckListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // بررسی آخرین وضعیت
                GetLatestStatus();
            }
        });

        // کلیک بر روی دکمه چک لیست های موقت
        tempCheckListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // رفتن به صفحه چک لیست های موقت
                Intent i = new Intent(getBaseContext(), TempCheckListActivity.class);
                startActivity(i);

            }
        });

        // کلیک بر روی دکمه ثبت ورود و خروج
        inputOutputUserLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // بررسی روشن بودن و دریافت لوکیشن
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (locationManager != null) {
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        // پیام درخواست روشن کردن جی پی اس
                        buildAlertMessageNoGps();

                    } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                        // دریافت موقعیت مکانی حال حاضر
                        getLocation();

                        // فراخوانی سرویس دریافت آخرین وضعیت ورود و خروج کاربر
                        final Call<LatestStatusResponse> createdRequest = new InputOutputUserOperations().GetService().LatestStatus();
                        createdRequest.enqueue(new Callback<LatestStatusResponse>() {
                            @Override
                            public void onResponse(Call<LatestStatusResponse> call, Response<LatestStatusResponse> response) {

                                if (response.isSuccessful()) {

                                    // کاربر وارد یک شعبه شده است و هنوز خروج نکرده است
                                    if (response.body().getOutputDateTime() == null || response.body().getOutputDateTime().equals(" ")) {

                                        // آپدیت اطلاعات ورود و خروج کاربر در کلاس عمومی
                                        GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();
                                        globalVariable.setLatestStatusResponse(response.body());

                                        // رفتن به صفحه ثبت خروج کاربر
                                        Intent i = new Intent(getBaseContext(), OutputUserActivity.class);
                                        startActivity(i);

                                    } else {

                                        // رفتن به صفحه لیست شعب و ثبت ورود کاربر
                                        Intent i = new Intent(getBaseContext(), InputUserActivity.class);
                                        startActivity(i);
                                    }
                                } else {

                                    // کاربر دارای هیچ شعبه ای نمی باشد
                                    // رفتن به صفحه لیست شعب و ثبت ورود کاربر
                                    Intent i = new Intent(getBaseContext(), InputUserActivity.class);
                                    startActivity(i);
                                    Log.e(mainActivity_TAG, response.message());
                                }
                            }

                            @Override
                            public void onFailure(Call<LatestStatusResponse> call, Throwable throwable) {
                            }
                        });
                    }
                }
            }
        });

        // کلیک بر روی دکمه لیست چک لیست ها
        checkListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // رفتن به صفحه لیست چک لیست ها
                Intent i = new Intent(getBaseContext(), UserCheckListActivity.class);
                startActivity(i);
            }
        });

        // کلیک بر روی دکمه خروج
        exit_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // پیام درخواست تایید خروج کاربر
                Dialog();
            }
        });
    }

    /**
     * متد درخواست تایید خروج کاربر
     */
    private void Dialog() {
        Typeface font = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile.ttf");
        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setMessage("آیا مایل به خروج از حساب کاربری هستید؟ (در صورت خروج، تمام داده های موقت و چک لیست های ارسال نشده شما حذف خواهد شد)")
                .setCancelable(true)
                .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SingletonAppBase.Clear(mActivity);
                        session.logoutUser();
                    }
                })
                .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();

        TextView textView = dialog.findViewById(android.R.id.message);
        if (textView != null) {
            textView.setTypeface(font);
        }
        Button btn1 = dialog.findViewById(android.R.id.button1);
        if (btn1 != null) {
            btn1.setTypeface(font);
        }
        Button btn2 = dialog.findViewById(android.R.id.button2);
        if (btn2 != null) {
            btn2.setTypeface(font);
        }
    }

    /**
     * متد دریافت مجوز موقعیت مکانی گوشی
     */
    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("لطفا GPS دستگاه خود را روشن کنید")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * متد دریافت موقعیت مکانی گوشی
     */
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            GlobalClass globalVariable = (GlobalClass) getApplication().getApplicationContext();

            if (location != null) {
                globalVariable.setLatitude(location.getLatitude());
                globalVariable.setLongitude(location.getLongitude());

            } else if (location1 != null) {
                globalVariable.setLatitude(location1.getLatitude());
                globalVariable.setLongitude(location1.getLongitude());

            } else if (location2 != null) {
                globalVariable.setLatitude(location2.getLatitude());
                globalVariable.setLongitude(location2.getLongitude());

            } else {
                view.showMessage("در حال حاضر، موقعیت مکانی شما در دسترس نمی باشد! شما می توانید با استفاده از نقشه ، موقعیت خود را ثبت نمایید.");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    @Override
    public void showWait() {
        SingletonProgressDialog.Start(mActivity);
    }

    @Override
    public void removeWait() {
        SingletonProgressDialog.Stop(mActivity);
    }

    @Override
    public void showEmpty() {

    }

    @Override
    public void hideEmpty() {

    }

    @Override
    public void retry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showMessage(String text) {
        content_main_layout.startAnimation(shakeAnimation);
        new CustomToast().Show_Toast(getApplicationContext(), content_main_layout, (text));
    }

    /**
     * دریافت اطلاعات کاربر و نمایش در ویو
     *
     * @param userProfile مدل اطلاعات کاربر
     */


    @Override
    public void getUserProfile(UserProfile userProfile) {

        //This corresponds to all of the information collected from AndroidManifest.xml.
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //get the app version Name for display
        String version = pInfo.versionName;
        //get the app version Code for checking
        int versionCode = pInfo.versionCode;


        //   ورژن جدید اپ
        if (userProfile.getAppVersionAndroid() != versionCode ) {

            Typeface font = Typeface.createFromAsset(this.getAssets(), "IRANSansMobile.ttf");
            AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                    .setMessage(" آیا مایل به دانلود نسخه جدید برنامه  هستید؟ ")
                    .setCancelable(true)
                    .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //TODO
                        }
                    })
                    .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();

            TextView textView = dialog.findViewById(android.R.id.message);
            if (textView != null) {
                textView.setTypeface(font);
            }
            Button btn1 = dialog.findViewById(android.R.id.button1);
            if (btn1 != null) {
                btn1.setTypeface(font);
            }
            Button btn2 = dialog.findViewById(android.R.id.button2);
            if (btn2 != null) {
                btn2.setTypeface(font);
            }

        }


        // نام کامل کاربر
        if (userProfile.getFirstName() != null && userProfile.getLastName() != null) {
            String fullName = userProfile.getFirstName() + "  " + userProfile.getLastName();
            firstNameLastName.setText(fullName);
        }



        // پیام های کاربر
        if (userProfile.getNewNotificationCount() != null) {
            ic_notifications.setImageResource(R.drawable.notifon);
            Log.e(mainActivity_TAG, "   نوتیف    " + userProfile.getNewNotificationCount() );
            try {


                ShortcutBadger.applyCount(context, userProfile.getNewNotificationCount().intValue()); //for 1.1.4+
                Log.e(mainActivity_TAG, "   setBadge  " + userProfile.getNewNotificationCount());

            } catch (Exception badgesNotSupportedException) {
                Log.e(mainActivity_TAG, "  badgesNotSupportedException fffffff  " + badgesNotSupportedException.getMessage());
            }

        }else {
            ic_notifications.setImageResource(R.drawable.notifoff);
            Log.e(mainActivity_TAG, "   نوتیف ناله   " );

        }

        // نقش های کاربر
        if (userProfile.getRoles().isEmpty()) {
            role.setText("کاربر");
        } else {
            StringBuilder roleText = new StringBuilder();
            for (Role role : userProfile.getRoles()) {
                roleText.append(role.getValue()).append("  ");
            }
            role.setText(roleText.toString());
        }

        // تصویر کاربر
        if (userProfile.getImageLastAddress() != null) {
            Picasso.with(getApplicationContext())
                    .load(userProfile.getImageLastAddress())
                    .error(R.drawable.defua)
                    .into(profile_image);
        }

        if (userProfile.getBranchs() != null) {
            branchs = userProfile.getBranchs();

        } else {
            Log.e(mainActivity_TAG, "  userProfile.getBranchs().isEmpty()  ");

        }
        if (userProfile.getPermissions() != null) {
            for (Permission p : userProfile.getPermissions()) {

                if (p.getValue().equalsIgnoreCase("UserCheckList")) {
                    UserCheckListPermissions = true;
                }

                if (p.getValue().equalsIgnoreCase("NotInPersonLogin")) {
                    NotInPersonLoginPermissions = true;
                }

                if (p.getValue().equalsIgnoreCase("UserNotifications")) {
                    UserNotificationsPermissions = true;
                }
            }

            Log.e(mainActivity_TAG, "  UserCheckListPermissions  " + UserCheckListPermissions);
            Log.e(mainActivity_TAG, "  NotInPersonLoginPermissions  " + NotInPersonLoginPermissions);
            Log.e(mainActivity_TAG, "  UserNotificationsPermissions  " + UserNotificationsPermissions);

        }


    }



    /**
     * دریافت آخرین وضعیت کاربر
     */
    public void GetLatestStatus() {

        // فراخوانی سرویس آخرین وضعیت کاربر
        final Call<LatestStatusResponse> createdRequest = new InputOutputUserOperations().GetService().LatestStatus();
        createdRequest.enqueue(new Callback<LatestStatusResponse>() {
            @Override
            public void onResponse(Call<LatestStatusResponse> call, Response<LatestStatusResponse> response) {
                if (response.isSuccessful()) {
                    latestStatusResponse = response.body();

                    // آپدیت شناسه شعبه ورود کرده در مدل عمومی
                    GlobalVariable.setBranchId(response.body().getBranchId());

                    // بررسی ثبت چک لیست تکراری
                    IsExistCheckList();
                } else {
                    view.showMessage("به قسمت ورود و خروج مراجعه کرده و یک شعبه انتخاب نمایید.");
                }
            }

            @Override
            public void onFailure(Call<LatestStatusResponse> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * بررسی وجود ثبت چک لیست تکراری
     */
    public void IsExistCheckList() {

        // فراخوانی سرویس بررسی چک لیست تکراری برای شعبه انتخاب شده
        final Call<Void> createdRequest = new UserCheckListOperations().GetService().IsExistCheckList(GlobalVariable.getBranchId());
        createdRequest.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {

                    // بررسی ثبت چک لیست در چک لیست های موقت
                    CheckBranchInDb();
                } else {
                    view.showMessage("برای شعبه انتخاب شده در این دوره یک چک لیست ثبت شده است.");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * بررسی وجود یک چک لیست در پایگاه داده با توجه به کد شعبه انتخاب شده
     */
    public void CheckBranchInDb() {
        // بررسی عدم وجود شعبه در چک لیست های موقت
        if (DBCheckListOp.IsNotExistByBranchId(GlobalVariable.getBranchId())) {

            // دریافت همه گروه های سوال
            GetAllQuestionsGroup();
        } else {
            view.showMessage("شما برای این شعبه یکبار چک لیست ثبت کرده اید و می توانید در چک لیست های موقت آن را ویرایش نمایید.");
        }
    }

    /**
     * دریافت همه گروه های سوالات
     */
    public void GetAllQuestionsGroup() {

        // فراخوانی سرویس دریافت همه گروه های سوالات
        final Call<List<Group>> createdRequest = new QuestionsOperations().GetService().GetAllGroup();
        createdRequest.enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {

                    // آپدیت گروه های سوالات در مدل عمومی
                    GlobalVariable.setGlobalQGL(response.body());

                    // دریافت همه سوالات
                    GetAllQuestionList();
                } else {
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                }
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * دریافت لیست همه سوالات
     */
    public void GetAllQuestionList() {

        // فراخوانی سرویس دریافت همه سوالات
        final Call<List<Question>> createdRequest = new QuestionsOperations().GetService().GetAllQuestion("");
        createdRequest.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                if (response.isSuccessful()) {

                    // آپدیت سوالات در مدل عمومی
                    GlobalVariable = (GlobalClass) context.getApplicationContext();
                    GlobalVariable.setGlobalQL(response.body());

                    // افزودن تمامی سوالات در پایگاه داده برای شعبه انتخابی
                    AddCheckListInDb();
                } else {
                    view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable throwable) {
                view.showMessage("عدم برقراری ارتباط با سرور، دوباره سعی کنید!");
            }
        });
    }

    /**
     * افزودن یک چک لیست موقت در پایگاه داده
     */
    public void AddCheckListInDb() {
        // دریافت سوالات گرفته شده از مدل عمومی
        List<Question> model = GlobalVariable.getGlobalQL();

        // تبدیل لیست سوالات به بایت
        byte[] globalData = ConvertClass.ToByteGlobalData(model);

        // ساخت مدل چک لیست موقت برای پایگاه داده
        CheckListInfo entity = new CheckListInfo(0, "", GlobalVariable.getBranchId(), "داده موقت", "هنوز ثبت نشده", latestStatusResponse.getBranchArea(), latestStatusResponse.getBranchesManagenemt(), latestStatusResponse.getBranchDomainName(), latestStatusResponse.getBranchName(), globalData);

        // افزودن چک لیست موقت در پایگاه داده
        int checkListInfoId = DBCheckListOp.Add(entity);

        // آپدیت کلید جدول چک لیست اضافه شده در پایگاه داده
        GlobalVariable.setCheckListInfoId(checkListInfoId);

        // رفتن به صفحه گروه سوالات
        GotoQuestionGroupActivity();
    }

    /**
     * رفتن به صفحه گروه سوالات
     */
    public void GotoQuestionGroupActivity() {
        Intent intent = new Intent(context, QuestionGroupActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("parent", "Main");
        intent.putExtras(bundle);
        startActivity(intent);
    }


}
