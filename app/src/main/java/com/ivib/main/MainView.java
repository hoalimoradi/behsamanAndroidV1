package com.ivib.main;

import com.ivib.model.UserProfile.UserProfile;

public interface MainView {
    void showWait();

    void removeWait();

    void showEmpty();

    void hideEmpty();

    void retry();

    void hideRetry();

    void showMessage(String text);

    void getUserProfile(UserProfile userProfile);
}
