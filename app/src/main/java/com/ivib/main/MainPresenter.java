package com.ivib.main;

import android.content.Context;

import com.ivib.data.remote.UserProfileOperations;
import com.ivib.model.UserProfile.UserProfile;
import com.ivib.utility.GlobalClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {
    private final Context context;
    private final MainView view;

    MainPresenter(Context context, MainView view) {
        this.context = context;
        this.view = view;
    }

    /**
     * دریافت اطلاعات کاربر
     */
    public void getUserProfile() {
        view.showWait();
        view.hideEmpty();
        view.hideRetry();

        // فراخوانی اطلاعات کاربر
        final Call<UserProfile> createdRequest = new UserProfileOperations().GetService().GetUserProfile();
        createdRequest.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.isSuccessful()) {
                    view.removeWait();

                    // پر کردن اطلاعات شعب کاربر در مدل عمومی
                    GlobalClass globalVariable = (GlobalClass) context.getApplicationContext();
                    globalVariable.setBranchList(response.body().getBranchs());

                    // ارسال اطلاعات دریافت شده به ویو
                    view.getUserProfile(response.body());
                } else {
                    view.removeWait();
                    view.retry();
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable throwable) {
                view.removeWait();
                view.retry();
            }
        });
    }
}
